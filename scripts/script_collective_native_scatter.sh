#!/bin/bash
# Indique au système que l'argument qui suit est le programme utilisé pour exécuter ce fichier
# En règle générale, les "#" servent à mettre en commentaire le texte qui suit comme ici
echo script starts
echo script starts > out.txt
touch out.txt
 echo "Test with algo native Ompi of collevtive $1" >> out.txt 
 for N in 96 960 9600 96000 960000 9600000 96000000 
 do
  echo "Test with $N data" >> out.txt
  for i in {1..10}
  do
   echo "mpirun -np 96 --map-by core --bind-to core:overload-allowed -x LD_LIBRARY_PATH=/home/mansouri/mpi-topology/Prototype/lib:/home/mansouri/libs/hwloc-git/lib/ $1 $N >> out.txt"
   mpirun -np 96 --map-by core --bind-to core:overload-allowed -x LD_LIBRARY_PATH=/home/mansouri/mpi-topology/Prototype/lib:/home/mansouri/libs/hwloc-git/lib/ $1 $N >> out.txt
  done
 done

echo script done 
exit 0
