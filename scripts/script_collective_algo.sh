#!/bin/bash
# args: 
# $1 = run-file path
# $2 = mca restriction (--mca coll_tuned_use_dynamic_rules 1 --mca coll_tuned_reduce_algorithm)
# $3 = Max algo number of runed collective (ex: reduce = 6)  
# $4 = Number of MPI processes
# $5 = max data_size (1 and more)
# $6 = number of iteration (1 and more)    
# example :  --mca coll_tuned_use_dynamic_rules 1 --mca coll_tuned_reduce_algorithm
 
echo script starts
echo script starts > $1_out.txt
touch $1_out.txt
for X in `seq 1 $3`
 do
 echo "Test with algo $X of $1" >> $1_out.txt 
 for (( N=1; N <= $5; N=10*$N )) 
 do
  echo "Test with $N data" >> $1_out.txt
  for i in `seq 1 $6`
  do
   echo "mpirun $2 $X -np $4 --map-by core --bind-to core:overload-allowed -x LD_LIBRARY_PATH=/home/mansouri/mpi-topology/Prototype/lib:/home/mansouri/libs/hwloc-git/lib/ $1 $N >> $1_out.txt"
   mpirun $2 $X -np $4 --map-by core --bind-to core:overload-allowed -x LD_LIBRARY_PATH=/home/mansouri/mpi-topology/Prototype/lib:/home/mansouri/libs/hwloc-git/lib/ $1 $N >> $1_out.txt
  done
 done
done

echo script done 
exit 0
