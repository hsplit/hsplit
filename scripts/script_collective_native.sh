#!/bin/bash
# Run a test with native algorithm of OpenMPI collectives 
# $1 = run file
# $2 = number of processes 
# $3 = number of iteration (1 and more)
# $4 = max data_size (1 and more)

echo script starts
echo script starts > $1_out.txt
touch $1_out.txt
 echo "Test with algo native Ompi of collevtive $1" >> $1_out.txt 
 for (( N=1; N <= $4; N=10*$N )) 
 do
  echo "Test with $N data" >> $1_out.txt
  for i in `seq 1 $3`
  do
   echo "mpirun -np $2 --map-by core --bind-to core:overload-allowed -x LD_LIBRARY_PATH=/home/mansouri/mpi-topology/Prototype/lib:/home/mansouri/libs/hwloc-git/lib/ $1 $N >> $1_out.txt"
   mpirun -np $2 --map-by core --bind-to core:overload-allowed -x LD_LIBRARY_PATH=/home/mansouri/mpi-topology/Prototype/lib:/home/mansouri/libs/hwloc-git/lib/ $1 $N >> $1_out.txt
  done
 done

echo script done 
exit 0
