/*
This file is part of Hsplit.

Hsplit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hsplit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hsplit.  If not, see <https://www.gnu.org/licenses/>.

* Copyright © 2016-2020 Inria.  All rights reserved.
* Copyright © 2016-2020 Bordeaux INP. All rights reserved.
* See COPYING in top-level directory.

*/

#ifndef LIB_HSPLIT_TYPES_H
#define LIB_HSPLIT_TYPES_H

#include "mpi.h"
#include <hwloc.h>
#include <hwloc/shmem.h>

#ifdef NETLOC_SUPPORT
#include <netloc.h>
#endif 

#ifdef HWLOC_LINUX_SYS
#include <hwloc/linux.h>
#endif

#define MPI_COMM_TYPE_HW_GUIDED       111
#define MPI_COMM_TYPE_RESOURCE_GUIDED 222
#define MPI_COMM_TYPE_HW_UNGUIDED     333
#define NODE_NAME_SIZE MPI_MAX_PROCESSOR_NAME
#define STR_SIZE 128
#define _NUM_LEVELS 64
#define KEY_LEN (MPI_MAX_INFO_VAL-1)

typedef char str_array_t[NODE_NAME_SIZE];

typedef struct proc_data {
  pid_t pid;
  str_array_t node_name;
} proc_data_t;

#ifdef NETLOC_SUPPORT
typedef struct network_topo {
  int ndims; /* Nombre de niveaux dans la hierarchie reseau */
  int *dims; /* nombre de noeuds par niveau, tableau de taille ndims */
  int *coords; /* coordonnees de chaque processus à chaque niveau, tableau de taille ndims */
} net_topo_t;
#endif

typedef struct shmem_data {
  char        name[STR_SIZE];
  size_t      length;
  void       *addr;
} shmem_data_t;

typedef struct init_data {
  MPI_Comm ref_comm;
  hwloc_topology_t hwloc_topology;
  hwloc_cpuset_t *proc_cpuset;
  proc_data_t *proc_data;
  int *node_locality;
  int local_rank;
  shmem_data_t shmem_data;
#ifdef NETLOC_SUPPORT
  net_topo_t my_net;
  int *net_coords;
#endif
} hinit_t;


typedef enum {
  VM_MAP_FILE      = 0,
  VM_MAP_ANONYMOUS = 1,
  VM_MAP_HEAP      = 2,
  VM_MAP_STACK     = 3,
  VM_MAP_OTHER     = 4 /* vsyscall/vdso/vvar shouldn't occur since we stop after stack */
} hwloc_vm_map_kind_t;

#endif
