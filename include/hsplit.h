/*
This file is part of Hsplit.

Hsplit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hsplit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hsplit.  If not, see <https://www.gnu.org/licenses/>.

* Copyright © 2016-2023 Inria.  All rights reserved.
* Copyright © 2016-2023 Bordeaux INP. All rights reserved.
* See COPYING in top-level directory.

*/			      

#ifndef LIB_HSPLIT_H
#define LIB_HSPLIT_H

#include "hsplit_types.h"

/* Misc */
int MPIX_Hinit(void);
int MPIX_Hfinalize(void);

/* Comms creation */
int MPIX_Comm_split_type(MPI_Comm comm, int split_type, int key, MPI_Info info, MPI_Comm*newcomm);
int MPIX_Comm_hsplit_with_roots(MPI_Comm comm, int split_type, MPI_Info info, MPI_Comm *newcomm, MPI_Comm *root_comm);
int MPIX_Comm_hsplit_with_global_roots(MPI_Comm comm, int split_type, MPI_Info info, MPI_Comm *newcomm, MPI_Comm *root_comm);
//int MPIX_Comm_hsplit_with_allroots(MPI_Comm comm, MPI_Info info, MPI_Comm *newcomm, MPI_Comm *root_comm);

/* Query */
int MPIX_Get_hw_resource_min(MPI_Comm comm,int size, int *ranks,char **type); 
int MPIX_Get_hw_domain_neighbours(MPI_Comm comm, int hops, int metric,MPI_Group *newgroup);
int MPIX_Get_hw_topo_group(MPI_Comm comm, int root, int hops, int metric,MPI_Group *newgroup);
int MPIX_Get_hw_topology_info(int *numlevels,MPI_Info info);
int MPIX_Get_hw_resource_types(MPI_Info *info); 
int MPI_Get_hw_resource_info(MPI_Info *hw_info); /* Not an MPIX_ routine since supported in MPI 4.1 */
#endif /* LIB_HSPLIT_H */
