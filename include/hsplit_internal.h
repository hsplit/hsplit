/*
This file is part of Hsplit.

Hsplit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hsplit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hsplit.  If not, see <https://www.gnu.org/licenses/>.

* Copyright © 2016-2018 Inria.  All rights reserved.
* Copyright © 2016-2018 Bordeaux INP. All rigts reserved.
* See COPYING in top-level directory.

*/


#ifndef LIB_HSPLIT_INTERNAL_H

#define LIB_HSPLIT_INTERNAL_H
#define _DEFAULT_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <alloca.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include "hsplit_types.h"

#define SIGNUM 7

static int siglist[SIGNUM] = {SIGINT,SIGSEGV,SIGABRT,SIGKILL,SIGTERM,SIGBUS,SIGFPE};

static hinit_t init_data;

static int *get_wranks(MPI_Comm comm,int numprocs);
#ifdef NETLOC_SUPPORT
static int get_net_level(MPI_Comm comm,int rank);
#endif
static int all_local_procs(MPI_Comm comm,int *locality,int *color);
static hwloc_obj_t get_obj(MPI_Comm comm);
static int find_hole(size_t *, size_t);
static void shmem_cleanup(void);
static void shmem_handler(int);


/*
#define ROOTS_IFACE
#define DEBUG
*/


#endif
