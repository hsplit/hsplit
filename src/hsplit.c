/*
This file is part of Hsplit.

Hsplit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hsplit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hsplit.  If not, see <https://www.gnu.org/licenses/>.

* Copyright © Inria 2016-2023.  All rights reserved.
* Copyright © Bordeaux INP 2016-2023. All rights reserved.
* See COPYING in top-level directory.

*/

#include "hsplit_internal.h"

/*****************************************************************************/
/*                               USER INTERFACE                              */
/*****************************************************************************/

#define PROVIDER_STR "hwloc://"

int MPIX_Hinit(void)
{
  MPI_Comm local_comm;
  int mpi_error = MPI_SUCCESS;
  int color = MPI_UNDEFINED;
  int numprocs = 0;  
  int rank;
  int resultlen;
  int hwloc_error;
  int idx;
  int fd;

  memset(&init_data,0,sizeof(hinit_t));
  init_data.ref_comm = MPI_COMM_WORLD;
  
  mpi_error = MPI_Comm_size(init_data.ref_comm,&numprocs);
  if (mpi_error != MPI_SUCCESS) return mpi_error;
  mpi_error = MPI_Comm_rank(init_data.ref_comm,&rank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;

  init_data.proc_data = calloc(numprocs,sizeof(proc_data_t));
  init_data.proc_data[rank].pid = getpid();
  mpi_error = MPI_Get_processor_name(init_data.proc_data[rank].node_name,&resultlen);
  if (mpi_error != MPI_SUCCESS) return mpi_error;

  mpi_error = MPI_Allgather(MPI_IN_PLACE,0,MPI_DATATYPE_NULL,
                            init_data.proc_data,
                            sizeof(proc_data_t),
                            MPI_BYTE,init_data.ref_comm);
  if (mpi_error != MPI_SUCCESS) return mpi_error;
  
  for(idx = 0 ; idx < numprocs ; idx++){
#ifdef DEBUG
    fprintf(stdout," ============> proc %i on node %s\n",idx,init_data.proc_data[idx].node_name);
#endif
    if (strcmp(init_data.proc_data[rank].node_name,
	       init_data.proc_data[idx].node_name) == 0){
      color = idx;
      break;
    }
  }
  mpi_error = MPI_Comm_split(init_data.ref_comm,color,rank,&local_comm);
  if (mpi_error != MPI_SUCCESS) return mpi_error;
  mpi_error = MPI_Comm_rank(local_comm,&init_data.local_rank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;

  if(0 == init_data.local_rank) {
#ifdef DEBUG
    fprintf(stdout,"Process %i is local master on node %i\n",rank,color);
#endif
    sprintf(init_data.shmem_data.name,"%s_%i_%i","/hsplit_common_hwloc_topoobj",getuid(),getpid());  
#ifdef DEBUG
    fprintf(stdout,"Shm obj name : %s \n",init_data.shmem_data.name);
#endif
    hwloc_error = hwloc_topology_init(&init_data.hwloc_topology);
    if (-1 == hwloc_error) return MPI_ERR_INTERN;

    hwloc_error = hwloc_topology_load(init_data.hwloc_topology);
    if (-1 == hwloc_error) return MPI_ERR_INTERN;

    hwloc_error = hwloc_shmem_topology_get_length(init_data.hwloc_topology,&init_data.shmem_data.length,0);
    if (-1 == hwloc_error) goto bcast;

    fd = shm_open(init_data.shmem_data.name, O_RDWR | O_CREAT | O_EXCL, S_IRWXU);
    if(fd > 0){      
      int ret = ftruncate(fd,init_data.shmem_data.length) ;
      if(ret != 0) {
	shm_unlink(init_data.shmem_data.name);
	init_data.shmem_data.length = 0;
      }
#ifdef DEBUG
      else
	fprintf(stdout,"=======> ftruncate success\n");
#endif
    } else 
      goto bcast;
    mpi_error = find_hole((size_t *)&init_data.shmem_data.addr, init_data.shmem_data.length);
    if (mpi_error != MPI_SUCCESS) {
#ifdef DEBUG
      fprintf(stdout,"==========> Hole not found (in Master)!\n");
#endif
      shm_unlink(init_data.shmem_data.name);
      init_data.shmem_data.length = 0;
    } else {
      hwloc_error = hwloc_shmem_topology_write(init_data.hwloc_topology,fd,0,(void *)init_data.shmem_data.addr,init_data.shmem_data.length,0);
      if (-1 == hwloc_error){
	shm_unlink(init_data.shmem_data.name);
	init_data.shmem_data.length = 0;
      }
      else {
	struct sigaction act;
	int idx;
	memset(&act,0,sizeof(struct sigaction));
	act.sa_handler = shmem_handler;
	for(idx = 0 ; idx < SIGNUM ; idx++)
	  sigaction(siglist[idx], &act, NULL);
	atexit(shmem_cleanup);
#ifdef DEBUG
	fprintf(stdout,"MASTER len is %lu, addr : %p\n\n",init_data.shmem_data.length,init_data.shmem_data.addr);
#endif
      }
    }
  bcast:
    mpi_error = MPI_Bcast(&init_data.shmem_data,sizeof(shmem_data_t),MPI_BYTE,0,local_comm);
    if (mpi_error != MPI_SUCCESS)
      shm_unlink(init_data.shmem_data.name);
  } else { /* not master on local comm (node)) */
    mpi_error = MPI_Bcast(&init_data.shmem_data,sizeof(shmem_data_t),MPI_BYTE,0,local_comm);
    if (mpi_error != MPI_SUCCESS)
      goto own_alloc;
    
    if (init_data.shmem_data.length > 0) {
      fd = shm_open(init_data.shmem_data.name, O_RDWR, S_IRWXU);
      if ( fd < 0)
	goto own_alloc;
      hwloc_error = hwloc_shmem_topology_adopt(&init_data.hwloc_topology,fd,0,(void *)init_data.shmem_data.addr,init_data.shmem_data.length,0);
      if (-1 == hwloc_error) {
      own_alloc:
#ifdef DEBUG
	fprintf(stdout,"Fallback to own allocation for topology data\n");
#endif
	shm_unlink(init_data.shmem_data.name);
	
	hwloc_error = hwloc_topology_init(&init_data.hwloc_topology);
	if (-1 == hwloc_error) return MPI_ERR_INTERN;
	
	hwloc_error = hwloc_topology_load(init_data.hwloc_topology);
	if (-1 == hwloc_error) return MPI_ERR_INTERN;
      }
      else {
	struct sigaction act;
	int idx;
	memset(&act,0,sizeof(struct sigaction));
	act.sa_handler = shmem_handler;
	for(idx = 0 ; idx < SIGNUM ; idx++)
	  sigaction(siglist[idx], &act,NULL);
	atexit(shmem_cleanup);
#ifdef DEBUG
	fprintf(stdout,"Adopt OK\n");
#endif
      } /* hwloc_error  != -1 */
    } /* init_data.shmem_data.length > 0 */
  } /* not master on local comm (node)) */

  init_data.node_locality = calloc(numprocs,sizeof(int));
  for(idx = 0 ; idx < numprocs ; idx++)
    if (idx != rank)
      if(strcmp(init_data.proc_data[rank].node_name,
                init_data.proc_data[idx].node_name)==0)
        init_data.node_locality[idx] = 1;
      else init_data.node_locality[idx] = 0;
    else init_data.node_locality[rank] = 1;

  init_data.proc_cpuset = malloc(numprocs*sizeof(hwloc_cpuset_t));
  for(idx = 0 ; idx < numprocs ; idx++){
    if(init_data.node_locality[idx]){
      init_data.proc_cpuset[idx] =  hwloc_bitmap_alloc();
      hwloc_error = hwloc_get_proc_cpubind(init_data.hwloc_topology,
                                           init_data.proc_data[idx].pid,
                                           init_data.proc_cpuset[idx],
                                           HWLOC_CPUBIND_PROCESS);
      if (-1 == hwloc_error) mpi_error = MPI_ERR_INTERN;
    }
  }
    
#ifdef NETLOC_SUPPORT
  hwloc_error = netloc_get_network_coords(&(init_data.my_net.ndims), &(init_data.my_net.dims), &(init_data.my_net.coords));
  if (hwloc_error != NETLOC_SUCCESS) return MPI_ERR_INTERN;
  /* printf("Netloc support activated| Network levels: %d \n", init_data.my_net.ndims); */
  /* remove last level: port number that we don't need */
  init_data.my_net.ndims--;
  
#ifdef DEBUG 
  if( init_data.my_net.ndims > 0){
    fprintf(stdout,"Network topology : number of levels : %i\n", init_data.my_net.ndims);
      for (idx = 0; idx < init_data.my_net.ndims; idx++) 
	fprintf(stdout,"Coords[level : %i] :%i/%i\n",idx,init_data.my_net.coords[idx],init_data.my_net.dims[idx]);
  }
#endif /* DEBUG */
#endif /* NETLOC_SUPPORT */
  
#ifdef DEBUG
  {
    char string[STR_SIZE];
    int topodepth;
    int idx,idx2;
    
    topodepth = hwloc_topology_get_depth(init_data.hwloc_topology);

    fprintf(stdout,"===============> topodepth : %i\n",topodepth);
    
    for(idx = 0 ; idx < topodepth ; idx++)
      {
	printf("*** Objects at level %d\n", idx);
	for (idx2 = 0; idx2 < hwloc_get_nbobjs_by_depth(init_data.hwloc_topology, idx); idx2++) {
	  hwloc_obj_type_snprintf(string,STR_SIZE,
				  hwloc_get_obj_by_depth(init_data.hwloc_topology, idx, idx2), 0);
	  printf("Index %u: %s\n", idx2, string);
	}
      }
  }
#endif
  
  
#ifdef NETLOC_SUPPORT
  init_data.net_coords = calloc(numprocs*(init_data.my_net.ndims),sizeof(int));
  memcpy(init_data.net_coords+init_data.my_net.ndims*rank,init_data.my_net.coords,init_data.my_net.ndims*sizeof(int));
  mpi_error = MPI_Allgather(MPI_IN_PLACE,0,MPI_DATATYPE_NULL,
			    init_data.net_coords,init_data.my_net.ndims,MPI_INT,init_data.ref_comm);
#endif   
  
#ifdef DEBUG
  for(idx = 0 ; idx < numprocs ; idx++)
    fprintf(stdout,"======= Info for procs[%i/COMM_WORLD] : %i %s \n",
	    idx,init_data.proc_data[idx].pid,
	    init_data.proc_data[idx].node_name);
  
  fprintf(stdout,"===== Node Locality :");
  for(idx = 0 ; idx < numprocs ; idx++)
    fprintf(stdout," %i ",init_data.node_locality[idx]);
  fprintf(stdout,"\n");
  
  fprintf(stdout,"===== Cpusets (COMM_WORLD) :");
  for(idx = 0 ; idx < numprocs ; idx++){
    if(init_data.node_locality[idx]){
      char str[STR_SIZE];
      hwloc_bitmap_snprintf(str,STR_SIZE,init_data.proc_cpuset[idx]);    
      fprintf(stdout," [%i | %s] ",idx,str);
    }
  }
  fprintf(stdout,"\n");    
  
#ifdef NETLOC_SUPPORT
  for(idx = 0 ; idx < numprocs ; idx++) {
    int idx2;
    fprintf(stdout,"==== Proc[%i] Network coordinates => ",idx);
    for(idx2 = 0 ; idx2 < init_data.my_net.ndims ; idx2++)
      fprintf(stdout," Level[%i] : %i/%i |",idx2,*(init_data.net_coords+init_data.my_net.ndims*idx+idx2),init_data.my_net.dims[idx2]);
    fprintf(stdout,"\n");
  }
#endif /* NETLOC */
#endif /* DEBUG  */
  
  return mpi_error;
}


int MPIX_Hfinalize(void)
{
  int idx;
  int size;
  
  MPI_Comm_size(init_data.ref_comm,&size);
  for(idx = 0 ; idx < size ; idx++)
    if(init_data.node_locality[idx])
      hwloc_bitmap_free(init_data.proc_cpuset[idx]);
  
#ifdef NETLOC_SUPPORT
  free(init_data.net_coords);
#endif
  free(init_data.proc_cpuset); 
  free(init_data.node_locality);
  free(init_data.proc_data);
  hwloc_topology_destroy(init_data.hwloc_topology);

  return MPI_SUCCESS;
}


int MPIX_Comm_split_type(MPI_Comm comm, int split_type, int key, MPI_Info info, MPI_Comm *newcomm)
{ 
  int mpi_error = MPI_SUCCESS;
  
  if((split_type != MPI_COMM_TYPE_HW_GUIDED) &&
     (split_type != MPI_COMM_TYPE_RESOURCE_GUIDED) &&
     (split_type != MPI_COMM_TYPE_HW_UNGUIDED)){
    return mpi_error = MPI_Comm_split_type(comm,split_type,key,info,newcomm);
  }
  else {
    hwloc_obj_type_t obj_type;
    hwloc_obj_t base_obj;
    char *str = NULL;
    int wrank; 
    int rank;
    int numprocs;
    int keylen = 0;
    int flag = 0;
    
    mpi_error = MPI_Comm_rank(init_data.ref_comm,&wrank);
    if (mpi_error != MPI_SUCCESS) return mpi_error;
    
    mpi_error = MPI_Comm_rank(comm,&rank);
    if (mpi_error != MPI_SUCCESS) return mpi_error;
    
    mpi_error = MPI_Comm_size(comm,&numprocs);
    if (mpi_error != MPI_SUCCESS) return mpi_error;
    
    if((split_type == MPI_COMM_TYPE_HW_GUIDED) ||
       (split_type == MPI_COMM_TYPE_RESOURCE_GUIDED)){
      if(info == MPI_INFO_NULL){
	*newcomm = MPI_COMM_NULL;
	return MPI_SUCCESS;	      
      } else {
	mpi_error = MPI_Info_get_valuelen(info,"mpi_hw_resource_type",&keylen,&flag);
	if (mpi_error != MPI_SUCCESS) return mpi_error;
	
	if(!flag){
	  *newcomm = MPI_COMM_NULL;
	  return MPI_SUCCESS;	      
	} else {
	  int split_color = 0;
	  str = calloc(keylen+1,sizeof(char));          
	  mpi_error = MPI_Info_get(info,"mpi_hw_resource_type",keylen,str,&flag);
	  if (mpi_error != MPI_SUCCESS) return mpi_error;
#ifdef DEBUG
	  fprintf(stdout,"====================> Got info val : %s | %i\n",str, mpi_error);
#endif
	  /* First, deal with type_shared */
	  /* Select nodes but this is not always the right answer */
	  /* depending on the network, newcomm could be a *duplicate* of comm */
	  if(strcmp(str,"mpi_shared_memory") == 0){
	    int all_local;
	    
	    mpi_error = all_local_procs(comm,&all_local,&split_color);
	    if (mpi_error != MPI_SUCCESS) return mpi_error;
	    
	    mpi_error = MPI_Comm_split(comm,split_color,key,newcomm);
	    if (mpi_error != MPI_SUCCESS) return mpi_error;
	    
	    if(*newcomm != MPI_COMM_NULL){
	      mpi_error = MPI_Info_set(info,"mpi_hw_resource_type","hwloc://Machine");
	      //goto next;
	    }
	  }
	  else 
#ifdef NETLOC_SUPPORT
		    /* Deal with network split */
	    if(strncasecmp(str,"net_level_",strlen("net_level_")) == 0){
	      int level  = str[strlen("net_Level_")]-'0';
	      split_color = init_data.my_net.coords[level];
#ifdef DEBUG
	      fprintf(stdout,"================================= split at net level : %i (color : %i)\n",level,split_color);
#endif
	      mpi_error = MPI_Comm_split(comm,split_color,key,newcomm);
	      if (mpi_error != MPI_SUCCESS) return mpi_error;
	      goto next;
	    }   
	    else 
#endif /* NETLOC_SUPPORT */
	      {
		/* Split inside node */		      
		/* FIXME: need to reimplement sscanf to strictly comply to spec */

		
		char *token = strsep(&str,":");/* Remove provider name from URI */
		if(str) str +=2;               /* Remove // */
		else str = token;		
		mpi_error = hwloc_type_sscanf(str,&obj_type,NULL,0);
		
		if (0 == mpi_error) {
		  mpi_error = hwloc_get_proc_cpubind(init_data.hwloc_topology,
						     init_data.proc_data[wrank].pid,
						     init_data.proc_cpuset[wrank],
						     HWLOC_CPUBIND_PROCESS);
		  if (-1 == mpi_error) return MPI_ERR_INTERN;
		  
		  base_obj = hwloc_get_next_obj_covering_cpuset_by_type(init_data.hwloc_topology,
									init_data.proc_cpuset[wrank],
									obj_type,
									NULL);
		  if(base_obj != NULL){
		    int all_local;
		    int color;
		    unsigned int numbits = 0;
		    unsigned int split_color = 0;
		    int higherbits = 0;
		    
		    /* we know locality, we just need the color */
		    mpi_error = all_local_procs(comm,&all_local,&color);
		    if (mpi_error != MPI_SUCCESS) return mpi_error;
		    
		    for (numbits = 0, higherbits = numprocs-1  ; higherbits ; higherbits >>= 1)
		      numbits++;			      
		    split_color  = base_obj->logical_index << numbits;
		    split_color |= (unsigned int)color;
		    if ((split_color & (0x1 << ((sizeof(int)*CHAR_BIT)-1))) >> ((sizeof(int)*CHAR_BIT)-1)){
#ifdef DEBUG
		      fprintf(stdout,"=========================================> Error in color : real %u (in split %i)\n",split_color,split_color);
#endif
		      *newcomm = MPI_COMM_NULL;
		      return MPI_ERR_INTERN;	       
		    }
		    
#ifdef DEBUG
		    fprintf(stdout,"Found obj number %i | color is %i\n",base_obj->logical_index,color);
		    fprintf(stdout,"===========================================> Split_color is %i\n",split_color);
#endif			      
		    mpi_error = MPI_Comm_split(comm,(int)split_color,key,newcomm); 
		    if (mpi_error != MPI_SUCCESS) return mpi_error;			      
		    
		    /* get the *real* resource name */
		    if(*newcomm != MPI_COMM_NULL) {
		      char str[STR_SIZE] = PROVIDER_STR;		      
		      hwloc_obj_type_snprintf(str+strlen(PROVIDER_STR),STR_SIZE,base_obj,0);
		      mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
		      if(mpi_error != MPI_SUCCESS) return mpi_error;
		    }
		  } else {
		    /* Specified type is unknown */
		    *newcomm = MPI_COMM_NULL;
		    return MPI_SUCCESS;	      
		  }  
		} else {
		  /* Specified type is unknown */
		  *newcomm = MPI_COMM_NULL;
		  return MPI_SUCCESS;	      
		}
	      }
	} /* flag == true => keyval found  */
      } /* info !=  NULL */
    } else if (split_type == MPI_COMM_TYPE_HW_UNGUIDED) {
      int all_local;
      int color;
#ifdef NETLOC_SUPPORT
      int level;
      /* Split on the first different level in switches */
      level = get_net_level(comm,rank);
      if (level == -1) return mpi_error;
      
      if(level < init_data.my_net.ndims) {
	/* processes are on different switches, split at this level*/
	color = init_data.my_net.coords[level];
	
	mpi_error = MPI_Comm_split(comm,color,key,newcomm); 
	if (mpi_error != MPI_SUCCESS) return mpi_error;
	
	if((*newcomm != MPI_COMM_NULL) && (info != MPI_INFO_NULL)){
	  char str[STR_SIZE];= PROVIDER_STR;
	  //memset(str,0,STR_SIZE);
	  mpi_error = sprintf(str+strlen(PROVIDER_STR),"net_level_%i",level);
	  if(mpi_error < 0) return MPI_ERR_INTERN;
	  
	  mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	  
	  goto next;
	} else
	  return mpi_error;	    
      } else {
	/* all processes are on the same switch but are they  */
	/* on the same node? */
	mpi_error = all_local_procs(comm,&all_local,&color);
	if (mpi_error != MPI_SUCCESS) return mpi_error;
      }     
#else /* NETLOC_SUPPORT */
      mpi_error = all_local_procs(comm,&all_local,&color);
      if (mpi_error != MPI_SUCCESS) return mpi_error;
#endif /* NETLOC_SUPPORT */
      
      if (!all_local){
	/* this creates as many comms as nodes */
	mpi_error = MPI_Comm_split(comm,color,key,newcomm); 
	if (mpi_error != MPI_SUCCESS) return mpi_error;
	
	if((*newcomm != MPI_COMM_NULL) && (info != MPI_INFO_NULL)){
	  char str[STR_SIZE] = PROVIDER_STR;
	  sprintf(str+strlen(PROVIDER_STR),hwloc_obj_type_string(get_obj(*newcomm)->type));	  
	  mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	} else
	  return mpi_error ;
      } else {
	/* All processes are local */
	hwloc_obj_t res_obj;
	int color = MPI_UNDEFINED;
	int wrank;
	unsigned int idx;
	
	res_obj = get_obj(comm);
	if(!res_obj) return MPI_ERR_INTERN;
	
	mpi_error = MPI_Comm_rank(init_data.ref_comm,&wrank);
	if (mpi_error != MPI_SUCCESS) return mpi_error;
	
	for(idx = 0; idx < res_obj->arity ; idx++)
	  if(hwloc_bitmap_isincluded(init_data.proc_cpuset[wrank],
				     res_obj->children[idx]->cpuset)){
	    color = idx;
	    break;
	  }
	
	/* we take the first child, go deeper until redundancies are handled */      
	if(color != MPI_UNDEFINED){
	  res_obj = res_obj->children[color];
	  while ((res_obj->arity == 1) && 
		 (hwloc_bitmap_isequal(res_obj->cpuset,res_obj->first_child->cpuset)))
	    res_obj = res_obj->first_child;
	}
	
	/* if color is undefined, newcomm is MPI_COMM_NULL */
	mpi_error = MPI_Comm_split(comm,color,key,newcomm);      
	if((*newcomm != MPI_COMM_NULL) && (info != MPI_INFO_NULL)) {
	  char str[STR_SIZE] = PROVIDER_STR;
	  /* then color is not MPI_UNDEFINED */
	  hwloc_obj_type_snprintf(str+strlen(PROVIDER_STR),STR_SIZE,res_obj,0);
	  mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	}
	else
	  return mpi_error;	    
      } /* all local */
    } /* unguided */
#ifdef NETLOC_SUPPORT
  next:
#endif
    if((*newcomm != MPI_COMM_NULL) && (info != MPI_INFO_NULL)) {
      MPI_Group g1,g2;
      int newrank = -1;
      int is_master = 0;
      int subcomms_num = 0;
      int subcomm_rank = 0;
      int in_rank = 0 ;
      int out_rank;
      int *values = NULL;
      int size;	  
      int idx;
      char str[STR_SIZE];
      
      mpi_error = MPI_Comm_rank(*newcomm,&newrank);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      
      if(!newrank)
	is_master++;
      
      mpi_error = MPI_Comm_size(comm,&size);	  
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      
      values = (int *)malloc(sizeof(int)*size);
      if(!values) return MPI_ERR_INTERN;
      
      values = memset(values,0,sizeof(int)*size);
      if(!values) return MPI_ERR_INTERN;
      
      values[rank] = is_master;
      
      mpi_error = MPI_Allgather(MPI_IN_PLACE,0,MPI_DATATYPE_NULL,values,1,MPI_INT,comm);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      
      /* to get the number of subcomms */
      /* count the number of masters (ie rank 0) */
      subcomms_num = 0;
      for(idx = 0; idx < size ; idx++)
	subcomms_num += values[idx];
      memset(str,0,STR_SIZE);
      mpi_error = sprintf(str,"%i",subcomms_num);
      if(mpi_error < 0) return MPI_ERR_INTERN;
      
      mpi_error = MPI_Info_set(info,"mpi_hw_resource_num",str);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      /* the rank of the subcomm is determined by the */
      /* rank of its master in the old comm           */
      mpi_error = MPI_Comm_group(comm,&g1);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      
      mpi_error = MPI_Comm_group(*newcomm,&g2);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      /* in_rank is 0 : master */
      mpi_error = MPI_Group_translate_ranks(g2,1,&in_rank,g1,&out_rank);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      
      for(idx = 0; idx < out_rank ; idx++)
	subcomm_rank += values[idx];
      memset(str,0,STR_SIZE);
      mpi_error = sprintf(str,"%i",subcomm_rank);
      if(mpi_error < 0) return MPI_ERR_INTERN;
      
      mpi_error = MPI_Info_set(info,"mpi_hw_resource_index",str);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      free(values);
    }
  }
  return mpi_error;	
}

/*
#ifdef REAL_IFACE
int MPIX_Get_hw_resource_info(MPI_Comm comm,int *num_subcomms,int *index,char **type)
#else 
int MPIX_Get_hw_resource_info(MPI_Comm comm,int *num_subcomms,int *index,char **type, MPI_Info info)
#endif
{
  int mpi_error = MPI_SUCCESS ;
#ifdef REAL_IFACE
  MPI_Info info = MPI_INFO_NULL;
  mpi_error = MPI_Comm_get_info(comm,&info);
  if(mpi_error != MPI_SUCCESS) return mpi_error;
#endif
  
  if(info != MPI_INFO_NULL)
    {
      int flag = 0;
      char *str = NULL;
      char *comm_name = calloc(STR_SIZE,sizeof(char));
      int keylen = 0;
      
      mpi_error = MPI_Info_get_valuelen(info,"mpi_hw_resource_num",&keylen,&flag);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      if(!flag) return MPI_ERR_INTERN;
      str = calloc(keylen+1,sizeof(char));
      mpi_error = MPI_Info_get(info,"mpi_hw_resource_num",keylen,str,&flag);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      if(num_subcomms != NULL){
	if (flag)
	  *num_subcomms = atoi(str);
	else
	  *num_subcomms = MPI_UNDEFINED;
      }
      free(str);
            
      mpi_error = MPI_Info_get_valuelen(info,"mpi_hw_resource_index",&keylen,&flag);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      if(!flag) return MPI_ERR_INTERN;
      str = calloc(keylen+1,sizeof(char));
      mpi_error = MPI_Info_get(info,"mpi_hw_resource_index",keylen,str,&flag);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      if(index != NULL){
	if (flag)
	  *index = atoi(str);
	else
	  *index = MPI_UNDEFINED;
      }
      free(str);
            
      mpi_error = MPI_Info_get_valuelen(info,"mpi_hw_resource_type",&keylen,&flag);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      if(!flag) return MPI_ERR_INTERN;
      str = calloc(keylen+1,sizeof(char));
      mpi_error = MPI_Info_get(info,"mpi_hw_resource_type",keylen,comm_name,&flag);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      if(type != NULL){
	if(flag)
	  *type = comm_name;
	else {
	  free(comm_name);
	  *type = NULL;
	}
      }
      else free(comm_name);
      free(str);
    }
  
  return mpi_error;
}
*/

int MPIX_Get_hw_resource_min(MPI_Comm comm,int n, int *ranks,char **type)
{
  MPI_Comm newcomm;
  MPI_Group group;
  MPI_Group newgroup;
  int newcomm_rank;
  int mpi_error = MPI_SUCCESS;
  char *comm_type = calloc(STR_SIZE,sizeof(char));

  mpi_error = MPI_Comm_group(comm,&group);
  if(mpi_error != MPI_SUCCESS) return mpi_error;
  
  mpi_error = MPI_Group_incl(group,n,ranks,&newgroup);
  if(mpi_error != MPI_SUCCESS) return mpi_error;
  
  mpi_error = MPI_Group_rank(newgroup,&newcomm_rank);
  if(mpi_error != MPI_SUCCESS) return mpi_error;

  mpi_error = MPI_Comm_create(comm,newgroup,&newcomm);
  if(mpi_error != MPI_SUCCESS) return mpi_error;  

  if(MPI_UNDEFINED == newcomm_rank)
    {
      /* this rank is not part of the communicator */
      strcpy(comm_type,"Unknown");
      *type = comm_type;
      return MPI_SUCCESS;
    } else {
    int is_local;
    
#ifdef NETLOC_SUPPORT
    int *lranks = NULL;
    int *wranks = NULL;    
    MPI_Group gworld;
    int level;
    int idx;
    
    mpi_error = MPI_Comm_group(init_data.ref_comm,&gworld);
    if(mpi_error != MPI_SUCCESS) return mpi_error;
    
    wranks = malloc(n*sizeof(int));
    lranks = malloc(n*sizeof(int)); 
    for(idx = 0 ; idx < n ; idx++)
      lranks[idx] = idx;
    mpi_error = MPI_Group_translate_ranks(newgroup,n,lranks,gworld,wranks);
    if(mpi_error != MPI_SUCCESS) return mpi_error;

    for(level  = 0 ; level < init_data.my_net.ndims ; level++) {
      int idx = 0;      
      while ((idx < n) && (((init_data.net_coords + wranks[idx]*init_data.my_net.ndims)[level]) == 
			   ((init_data.net_coords + wranks[newcomm_rank]*init_data.my_net.ndims)[level]))){
	  idx++;
      }
      if(idx != n)
	break;
    }
  
    if(level < init_data.my_net.ndims) {
      char str[STR_SIZE];
      sprintf(str,"Net_level_%i",level);      
#ifdef DEBUG
      fprintf(stdout,"====================>>>>>> %s\n",str);
#endif      
      free(wranks);
      free(lranks);
      strcpy(comm_type,str);
      *type = comm_type;	          
      return mpi_error;
    } else {
      /* all processes belong to the same switch */
      /* check if they are on the same node */
#ifdef DEBUG
      fprintf(stdout,"========== Last Level Switch (%i)!\n",level);
#endif
      is_local = 0;
      for(idx = 0 ; idx < n ; idx++)
	is_local += init_data.node_locality[wranks[idx]];
      
      is_local = ((is_local == n) ? 1 : 0);
            
      free(wranks);
      free(lranks);
    }
#else /* NETLOC_SUPPORT */
    int color;
    /* color is unused in this function */    
    mpi_error = all_local_procs(newcomm,&is_local,&color);
    if (mpi_error != MPI_SUCCESS) return mpi_error;
#endif /* NETLOC_SUPPORT */    
    
#ifdef DEBUG
    fprintf(stdout,"========== locality : %i\n",is_local);
#endif

    if (!is_local) {
#ifndef NETLOC_SUPPORT
      strcpy(comm_type,"System");
#else /* NETLOC_SUPPORT */
      strcpy(comm_type,"Net_level_0");
#endif /* NETLOC_SUPPORT */
      *type = comm_type;
      return MPI_SUCCESS;
    } else {
      hwloc_obj_t root_obj;
      int wrank;      

      mpi_error = MPI_Comm_rank(init_data.ref_comm,&wrank);
      if (mpi_error != MPI_SUCCESS) return mpi_error;
      
      root_obj = hwloc_get_root_obj(init_data.hwloc_topology);
      if(!root_obj) return MPI_ERR_INTERN;
      
      /* if cpubind returns an error, it will be full anyway */
      if(hwloc_bitmap_isincluded(root_obj->cpuset,
				 init_data.proc_cpuset[wrank])){
      /* processes are not bound on the machine */
	strcpy(comm_type,"Machine");
#ifdef DEBUG
	fprintf(stdout,"Unbound case\n");
#endif
      } else {
	/* all procs are bound */
	hwloc_obj_t res_obj;
             
	res_obj = get_obj(newcomm);
	if(!res_obj) return MPI_ERR_INTERN;      

	hwloc_obj_type_snprintf (comm_type,STR_SIZE,res_obj,0);
      }
      *type = comm_type;	    
    }
  }
  
  return mpi_error;
}


int MPIX_Get_hw_domain_neighbours(MPI_Comm comm, int hops, int metric,MPI_Group *newgroup)
{
  MPI_Group gcomm;
  hwloc_obj_t base_obj;
  hwloc_cpuset_t proc_cpuset ;
  int *proc_array = NULL;
  int *wranks = NULL;
  int numprocs;
  int rank;
  int wrank;
  int step = 0;
  int idx;
  int idx2 = 0;
#ifdef NETLOC_SUPPORT
  int idx3;
#endif
  int mpi_error = MPI_SUCCESS;
  int size = 0;
  
#ifdef DEBUG
  char *str = calloc(STR_SIZE,sizeof(char));
#endif

  mpi_error = MPI_Comm_rank(init_data.ref_comm,&wrank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;

  mpi_error = MPI_Comm_rank(comm,&rank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;

  mpi_error = MPI_Comm_size(comm,&numprocs);
  if (mpi_error != MPI_SUCCESS) return mpi_error;

  proc_cpuset = init_data.proc_cpuset[wrank];
  base_obj = hwloc_get_obj_covering_cpuset(init_data.hwloc_topology,proc_cpuset);
  if(!base_obj) return MPI_ERR_INTERN;
#ifdef DEBUG
  hwloc_obj_type_snprintf (str,MPI_MAX_INFO_VAL,base_obj,0);
  fprintf(stdout,"================> Calling proc obj type :%s\n",str);
#endif
  /* remove redundancies */
  while( (base_obj->parent) &&
	 (hwloc_bitmap_isequal(base_obj->parent->cpuset,base_obj->cpuset))) {
    base_obj = base_obj->parent;
  }

  while(step < hops)
    {
      if(base_obj->parent){ /* inside node */
	base_obj = base_obj->parent;
	/* remove redundancies */
	while((base_obj->parent)
	      &&(hwloc_bitmap_isequal(base_obj->parent->cpuset,base_obj->cpuset))){
	  base_obj = base_obj->parent;
	}
      } else { /* up in the network */
#ifdef DEBUG
	fprintf(stdout,"===============> Hopping outside node (rank %i) !\n",rank);
#endif
#ifdef NETLOC_SUPPORT
	int net_step = hops - step;
	wranks = get_wranks(comm,numprocs);
	assert(wrank == wranks[rank]);
	
	for(idx = init_data.my_net.ndims - 1; (idx >= 0) && (step < hops) ; idx-- , step++){
	  int idx2;
	  for(idx2 = 0 ; idx2 < numprocs ; idx2++) {
	    if(/* (idx2 != rank) && */
	       (((!init_data.node_locality[wranks[idx2]]) &&
		 ((init_data.net_coords+init_data.my_net.ndims*wranks[idx2])[idx] == init_data.my_net.coords[idx])) ||
		((init_data.node_locality[wranks[idx2]]) &&
		 hwloc_bitmap_isincluded(init_data.proc_cpuset[wranks[idx2]],base_obj->cpuset)))){
	      size++;
	    }
	  }
	}
#ifdef DEBUG
	fprintf(stdout,"************************* Neighborhood size is %i (step %i)\n",size,step);
#endif
	idx2 = 0;
	proc_array = (int *)alloca(size*sizeof(int));
	for(idx = 0 ; idx < numprocs ; idx++) {
	  /*  if(idx != rank){ */
#ifdef DEBUG
	    fprintf(stdout,">>>>>>>>>>>>> Checking for proc %i : locality %i\n",idx,init_data.node_locality[wranks[idx]]);
	    if (!init_data.node_locality[wranks[idx]])
	      for(idx3 = init_data.my_net.ndims - 1 ; idx3 >= 0 ; idx3--){
		fprintf(stdout,">> Net coords[level %i] : %i | ",idx3,(init_data.net_coords+init_data.my_net.ndims*wranks[idx])[idx3]);
		fprintf(stdout,"\n");
	      }
#endif
	    if(init_data.node_locality[wranks[idx]]){
	      if (hwloc_bitmap_isincluded(init_data.proc_cpuset[wranks[idx]],base_obj->cpuset)){
#ifdef DEBUG
		int pos = idx2;
		fprintf(stdout,"======== Adding local proc %i to array at pos %i\n",idx,pos);
#endif
		proc_array[idx2++] = idx;
	      }
	    } else {
	      int num  = net_step;
#ifdef DEBUG
	      fprintf(stdout,"Net_step to perform : %i\n",net_step);
#endif
	      for(idx3 = init_data.my_net.ndims - 1 ; (idx3  >= 0) && (num > 0); idx3--,num--){
		if((init_data.net_coords+init_data.my_net.ndims*wranks[idx])[idx3] == init_data.my_net.coords[idx3]){
#ifdef DEBUG
		  int pos = idx2;
		  fprintf(stdout,"======== Adding remote proc %i to array at pos %i\n",idx,pos);
#endif
		  proc_array[idx2++] = idx;
		}
	      }
	    }
	    /* } */
	}
	goto end;	
#else /* NETLOC_SUPPORT */
	/* out of the node and no network support: all processes are in the neighborhood*/
	/* the newgroup is the group of  the input comm */

	mpi_error = MPI_Comm_group(comm,newgroup);
	if (mpi_error != MPI_SUCCESS) return mpi_error;	

	return mpi_error;

#endif /* NETLOC_SUPPORT */
      }
      step++;
    }

#ifdef DEBUG
  hwloc_obj_type_snprintf (str,MPI_MAX_INFO_VAL,base_obj,0);
  fprintf(stdout,"================> Calling proc obj type at distance %i :%s\n",hops,str);
#endif
  
  wranks = get_wranks(comm,numprocs);
  assert(wrank == wranks[rank]);
  /* compute the array size */
  for(idx = 0 ; idx < numprocs; idx++){
    /* if(idx != rank){ */
      if(init_data.node_locality[wranks[idx]] &&
	 hwloc_bitmap_isincluded(init_data.proc_cpuset[wranks[idx]],base_obj->cpuset)){
	size++;
      }
      /* } */
  }
#ifdef DEBUG
  fprintf(stdout,"Size is %i\n",size);
#endif
  proc_array = (int *)alloca(size*sizeof(int));
  for(idx = 0 ; idx < numprocs; idx++){
    /* if(idx != rank){ */
    if(init_data.node_locality[wranks[idx]] &&
       hwloc_bitmap_isincluded(init_data.proc_cpuset[wranks[idx]],base_obj->cpuset)){
      proc_array[idx2++] = idx;
    }
    /*  } */
  }

#ifdef NETLOC_SUPPORT
 end:
#endif
#ifdef DEBUG
  fprintf(stdout,"Global (Top-level comm/MPI_COMM_WORLD) ranks :");
  for(idx = 0 ; idx < size ; idx++)
    fprintf(stdout,"** Proc[%i] : %i **",idx,wranks[proc_array[idx]]);
  fprintf(stdout,"\n");
#endif 

  free(wranks);    

  mpi_error = MPI_Comm_group(comm,&gcomm);
  if(mpi_error != MPI_SUCCESS) return mpi_error;  

  mpi_error = MPI_Group_incl(gcomm,size,proc_array,newgroup);
  if(mpi_error != MPI_SUCCESS) return mpi_error;

#ifdef DEBUG
  {
    int *lranks = NULL;
    int *lranks2 = NULL;

    lranks = (int *)alloca(size*sizeof(int));
    lranks2 = (int *)alloca(size*sizeof(int));
    for(idx = 0 ; idx < size ; idx++)
      lranks2[idx] = idx;
  
    MPI_Group_translate_ranks(*newgroup,size,lranks2,gcomm,lranks);
    
    fprintf(stdout,"Ranks in input comm :");
    for(idx = 0 ; idx < size ; idx++)
      fprintf(stdout,"== Proc[%i] : %i ==",idx,lranks[idx]);
    fprintf(stdout,"\n");
  }
#endif 

  return mpi_error;
}


int MPIX_Get_hw_topo_group(MPI_Comm comm, int root, int hops, int metric,MPI_Group *newgroup)
{
  MPI_Group gcomm;
  hwloc_obj_t base_obj;
  hwloc_cpuset_t proc_cpuset ;
  int *proc_array = NULL;
  int *wranks = NULL;
  int numprocs;
  int rank;
  int wrank;
  int wrank_root;
  int step = 0;
  int idx;
  int idx2 = 0;
#ifdef NETLOC_SUPPORT
  int idx3;
#endif
  int mpi_error = MPI_SUCCESS;
  int size = 0;
  
#ifdef DEBUG
  char *str = calloc(MPI_MAX_INFO_VAL,sizeof(char));
#endif

  mpi_error = MPI_Comm_rank(init_data.ref_comm,&wrank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;

  mpi_error = MPI_Comm_rank(comm,&rank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;

  mpi_error = MPI_Comm_size(comm,&numprocs);
  if (mpi_error != MPI_SUCCESS) return mpi_error;

  wranks = get_wranks(comm,numprocs);
  assert(wrank == wranks[rank]);

  wrank_root = wranks[root];
  proc_cpuset = init_data.proc_cpuset[wrank_root];
  //  proc_cpuset = init_data.proc_cpuset[wrank];
  base_obj = hwloc_get_obj_covering_cpuset(init_data.hwloc_topology,proc_cpuset);
  if(!base_obj) return MPI_ERR_INTERN;
#ifdef DEBUG
  hwloc_obj_type_snprintf (str,MPI_MAX_INFO_VAL,base_obj,0);
  fprintf(stdout,"================> Calling proc obj type :%s\n",str);
#endif
  /* remove redundancies */
  while( (base_obj->parent) &&
	 (hwloc_bitmap_isequal(base_obj->parent->cpuset,base_obj->cpuset))) {
    base_obj = base_obj->parent;
  }

  while(step < hops)
    {
      if(base_obj->parent){ /* inside node */
	base_obj = base_obj->parent;
	/* remove redundancies */
	while((base_obj->parent)
	      &&(hwloc_bitmap_isequal(base_obj->parent->cpuset,base_obj->cpuset))){
	  base_obj = base_obj->parent;
	}
      } else { /* up in the network */
#ifdef DEBUG
	fprintf(stdout,"===============> Hopping outside node (rank %i) !\n",rank);
#endif
#ifdef NETLOC_SUPPORT
	int net_step = hops - step;
	for(idx = init_data.my_net.ndims - 1; (idx >= 0) && (step < hops) ; idx-- , step++){
	  int idx2;
	  for(idx2 = 0 ; idx2 < numprocs ; idx2++) {
	    if(/* (idx2 != rank) && */
	       (((!init_data.node_locality[wranks[idx2]]) &&
		 ((init_data.net_coords+init_data.my_net.ndims*wranks[idx2])[idx] == init_data.my_net.coords[idx])) ||
		((init_data.node_locality[wranks[idx2]]) &&
		 hwloc_bitmap_isincluded(init_data.proc_cpuset[wranks[idx2]],base_obj->cpuset)))){
	      size++;
	    }
	  }
	}
#ifdef DEBUG
	fprintf(stdout,"************************* Neighborhood size is %i (step %i)\n",size,step);
#endif
	idx2 = 0;
	proc_array = (int *)alloca(size*sizeof(int));
	for(idx = 0 ; idx < numprocs ; idx++) {
	  /*  if(idx != rank){ */
#ifdef DEBUG
	    fprintf(stdout,">>>>>>>>>>>>> Checking for proc %i : locality %i\n",idx,init_data.node_locality[wranks[idx]]);
	    if (!init_data.node_locality[wranks[idx]])
	      for(idx3 = init_data.my_net.ndims - 1 ; idx3 >= 0 ; idx3--){
		fprintf(stdout,">> Net coords[level %i] : %i | ",idx3,(init_data.net_coords+init_data.my_net.ndims*wranks[idx])[idx3]);
		fprintf(stdout,"\n");
	      }
#endif
	    if(init_data.node_locality[wranks[idx]]){
	      if (hwloc_bitmap_isincluded(init_data.proc_cpuset[wranks[idx]],base_obj->cpuset)){
#ifdef DEBUG
		int pos = idx2;
		fprintf(stdout,"======== Adding local proc %i to array at pos %i\n",idx,pos);
#endif
		proc_array[idx2++] = idx;
	      }
	    } else {
	      int num  = net_step;
#ifdef DEBUG
	      fprintf(stdout,"Net_step to perform : %i\n",net_step);
#endif
	      for(idx3 = init_data.my_net.ndims - 1 ; (idx3  >= 0) && (num > 0); idx3--,num--){
		if((init_data.net_coords+init_data.my_net.ndims*wranks[idx])[idx3] == init_data.my_net.coords[idx3]){
#ifdef DEBUG
		  int pos = idx2;
		  fprintf(stdout,"======== Adding remote proc %i to array at pos %i\n",idx,pos);
#endif
		  proc_array[idx2++] = idx;
		}
	      }
	    }
	    /* } */
	}
	goto end;	
#else /* NETLOC_SUPPORT */
	/* out of the node and no network support: all processes are in the neighborhood*/
	/* the newgroup is the group of  the input comm */

	mpi_error = MPI_Comm_group(comm,newgroup);
	if (mpi_error != MPI_SUCCESS) return mpi_error;	

	return mpi_error;

#endif /* NETLOC_SUPPORT */
      }
      step++;
    }

#ifdef DEBUG
  hwloc_obj_type_snprintf (str,MPI_MAX_INFO_VAL,base_obj,0);
  fprintf(stdout,"================> Calling proc obj type at distance %i :%s\n",hops,str);
#endif
  /* compute the array size */
  for(idx = 0 ; idx < numprocs; idx++){
    /* if(idx != rank){ */
      if(init_data.node_locality[wranks[idx]] &&
	 hwloc_bitmap_isincluded(init_data.proc_cpuset[wranks[idx]],base_obj->cpuset)){
	size++;
      }
      /* } */
  }
#ifdef DEBUG
  fprintf(stdout,"Size is %i\n",size);
#endif
  proc_array = (int *)alloca(size*sizeof(int));
  for(idx = 0 ; idx < numprocs; idx++){
    /* if(idx != rank){ */
    if(init_data.node_locality[wranks[idx]] &&
       hwloc_bitmap_isincluded(init_data.proc_cpuset[wranks[idx]],base_obj->cpuset)){
      proc_array[idx2++] = idx;
    }
    /*  } */
  }

#ifdef NETLOC_SUPPORT
 end:
#endif
#ifdef DEBUG
  fprintf(stdout,"Global (Top-level comm/MPI_COMM_WORLD) ranks :");
  for(idx = 0 ; idx < size ; idx++)
    fprintf(stdout,"** Proc[%i] : %i **",idx,wranks[proc_array[idx]]);
  fprintf(stdout,"\n");
#endif 

  free(wranks);    

  mpi_error = MPI_Comm_group(comm,&gcomm);
  if(mpi_error != MPI_SUCCESS) return mpi_error;  

  mpi_error = MPI_Group_incl(gcomm,size,proc_array,newgroup);
  if(mpi_error != MPI_SUCCESS) return mpi_error;

#ifdef DEBUG
  {
    int *lranks = NULL;
    int *lranks2 = NULL;

    lranks = (int *)alloca(size*sizeof(int));
    lranks2 = (int *)alloca(size*sizeof(int));
    for(idx = 0 ; idx < size ; idx++)
      lranks2[idx] = idx;
  
    MPI_Group_translate_ranks(*newgroup,size,lranks2,gcomm,lranks);
    
    fprintf(stdout,"Ranks in input comm :");
    for(idx = 0 ; idx < size ; idx++)
      fprintf(stdout,"== Proc[%i] : %i ==",idx,lranks[idx]);
    fprintf(stdout,"\n");
  }
#endif 

  return mpi_error;
}

int MPIX_Get_hw_topology_info(int *numlevels, MPI_Info info)
{
  hwloc_obj_t base_obj;
  hwloc_obj_t mach_obj;
  int mpi_error = MPI_SUCCESS;
  int wrank;
  int nlevels = 0;
#ifdef NETLOC_SUPPORT
  int idx;
#endif
  char str[STR_SIZE];
  char str2[STR_SIZE];
  
  mpi_error = MPI_Comm_rank(init_data.ref_comm,&wrank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;

  /* refresh binding */ 
  mpi_error = hwloc_get_proc_cpubind(init_data.hwloc_topology,
				     init_data.proc_data[wrank].pid,
				     init_data.proc_cpuset[wrank],
				     HWLOC_CPUBIND_PROCESS);      
  if (-1 == mpi_error) return MPI_ERR_INTERN;

  base_obj = hwloc_get_obj_covering_cpuset(init_data.hwloc_topology,
					   init_data.proc_cpuset[wrank]);
  if(base_obj) 
    {
      sprintf(str,"MPI_HW_LEVEL%d",nlevels);
      hwloc_obj_type_snprintf (str2,STR_SIZE,base_obj,0);
#ifdef DEBUG
      fprintf(stdout,"================> %s type :%s\n",str,str2);
#endif    
      mpi_error = MPI_Info_set(info,str,str2);
      if(mpi_error != MPI_SUCCESS) return mpi_error;	  
            
      while(base_obj->parent){
      /* remove redundancies */
	while((base_obj->parent) && (hwloc_bitmap_isequal(base_obj->parent->cpuset,base_obj->cpuset)))
	  base_obj = base_obj->parent;
	
	if(base_obj->parent){
	  base_obj = base_obj->parent;
	  nlevels++;	  
	  sprintf(str,"MPI_HW_LEVEL%d",nlevels);
	  hwloc_obj_type_snprintf (str2,STR_SIZE,base_obj,0);
#ifdef DEBUG
	  fprintf(stdout,"================> %s type :%s\n",str,str2);
#endif    
	  mpi_error = MPI_Info_set(info,str,str2);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;	
	}	
      }
    }   
  
  mach_obj = hwloc_get_obj_by_type(init_data.hwloc_topology,HWLOC_OBJ_MACHINE,0);
  if (!hwloc_bitmap_isequal(base_obj->cpuset, mach_obj->cpuset)){
    nlevels++;	  
    sprintf(str,"MPI_HW_LEVEL%d",nlevels);
    mpi_error = MPI_Info_set(info,str,"Machine");
    if(mpi_error != MPI_SUCCESS) return mpi_error;	  
#ifdef DEBUG
    fprintf(stdout,"================> %s type :%s\n",str,"Machine");
#endif    
  }
  
#ifdef NETLOC_SUPPORT
  for(idx = 0 ; idx < init_data.my_net.ndims ; idx++)
    {
      nlevels++;      
      sprintf(str,"MPI_HW_LEVEL%d",nlevels);
      sprintf(str2,"Net_level_%d",init_data.my_net.ndims-idx-1);

      mpi_error = MPI_Info_set(info,str,str2);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
#ifdef DEBUG
      fprintf(stdout,"================> Level type : Net level %i\n",idx);
#endif
    }
#ifdef DEBUG
  fprintf(stdout,"Number of levels : %i / (net levels %i)\n",nlevels,init_data.my_net.ndims);
#endif

#endif /*NETLOC_SUPPORT*/

  *numlevels = ++nlevels;

  return mpi_error;
}

int MPI_Get_hw_resource_info(MPI_Info *hw_info)
{
  int mpi_error = MPI_SUCCESS;
  int nlevels = 0;
  int nlevels_node = 0;
  int wrank;
  char str[STR_SIZE];
  
  mpi_error = MPI_Info_create(hw_info);
  if(mpi_error != MPI_SUCCESS) return mpi_error;

  mpi_error =  MPI_Comm_rank(init_data.ref_comm,&wrank);
  if(mpi_error != MPI_SUCCESS) return mpi_error;
  
  /* refresh binding */ 
  mpi_error = hwloc_get_proc_cpubind(init_data.hwloc_topology,
				     init_data.proc_data[wrank].pid,
				     init_data.proc_cpuset[wrank],
				     HWLOC_CPUBIND_PROCESS);      
  if (-1 == mpi_error) return MPI_ERR_INTERN;
  
  nlevels_node = hwloc_topology_get_depth(init_data.hwloc_topology);  
  nlevels = nlevels_node;
#ifdef NETLOC_SUPPORT
  nlevels += init_data.my_net.ndims;
#ifdef DEBUG
  fprintf(stdout,"==============> topo depth is %i (net %i, node %i)\n ",nlevels,init_data.my_net.ndims,nlevels_node );
#endif
  for(current_level = 0 ; current_level < init_data.my_net.ndims ; current_level++)
    {
      memset(str,0,STR_SIZE);
      sprintf(str,"netloc://net_level_%i",current_level);
      /* obviously */
      mpi_error = MPI_Info_set(*hw_info,str,"true");
      if(mpi_error != MPI_SUCCESS) return mpi_error;
    }
#else
#ifdef DEBUG
  fprintf(stdout,"==============> topo depth is %i (All node)\n ",nlevels);
#endif
#endif /*NETLOC_SUPPORT*/

  int numa_depth = hwloc_get_memory_parents_depth(init_data.hwloc_topology);
#ifdef DEBUG
  fprintf(stdout,"==============> NumaNode @ depth %i \n ",numa_depth);
#endif

  if( numa_depth > 0){ // Numa Objects are all on the same level
    int level = 0;
    int is_bound = 0;
    hwloc_obj_t numa_obj;

    // Add all objects on levels before NUMA
    for( level = 0 ; level <= numa_depth ; level++ ){
      hwloc_obj_t next_obj = hwloc_get_next_obj_by_depth(init_data.hwloc_topology,level,NULL);
      is_bound = 0;

      memset(str,0,STR_SIZE);
      sprintf(str,"hwloc://%s",hwloc_obj_type_string(next_obj->type));

      do{
	if(hwloc_bitmap_isincluded(init_data.proc_cpuset[wrank],next_obj->cpuset)){
	  is_bound = 1;
	  break;
	}       
      } while((next_obj = hwloc_get_next_obj_by_depth(init_data.hwloc_topology,level,next_obj)));

      mpi_error = MPI_Info_set(*hw_info,str,is_bound ? "true" : "false");
      if(mpi_error != MPI_SUCCESS) return mpi_error;
    }

    // Add NUMANodes
    numa_obj = hwloc_get_next_obj_by_depth(init_data.hwloc_topology,numa_depth,NULL); 
    if(hwloc_bitmap_isincluded(init_data.proc_cpuset[wrank],numa_obj->cpuset)){
      is_bound = 1;
    }
    mpi_error = MPI_Info_set(*hw_info,"hwloc://NUMANode",is_bound ? "true" : "false");
    if(mpi_error != MPI_SUCCESS) return mpi_error;

    // Add all objects on levels after NUMA
    for( level = numa_depth + 1 ; level < nlevels ; level++ ){
      hwloc_obj_t next_obj = hwloc_get_next_obj_by_depth(init_data.hwloc_topology,level,NULL);
      is_bound = 0;

      memset(str,0,STR_SIZE);
      sprintf(str,"hwloc://%s",hwloc_obj_type_string(next_obj->type));

      do{
	if(hwloc_bitmap_isincluded(init_data.proc_cpuset[wrank],next_obj->cpuset)){
	  is_bound = 1;
	  break;
	}       
      } while((next_obj = hwloc_get_next_obj_by_depth(init_data.hwloc_topology,level,next_obj)));

      mpi_error = MPI_Info_set(*hw_info,str,is_bound ? "true" : "false");
      if(mpi_error != MPI_SUCCESS) return mpi_error;
    }
  } else {    
    // Add all objects
    int is_bound = 0;
    hwloc_obj_t numa_obj;

    for( int level = 0 ; level < nlevels ; level++ ){
      hwloc_obj_t next_obj = hwloc_get_next_obj_by_depth(init_data.hwloc_topology,level,NULL);
      is_bound = 0;

      memset(str,0,STR_SIZE);
      sprintf(str,"hwloc://%s",hwloc_obj_type_string(next_obj->type));

      do{
	if(hwloc_bitmap_isincluded(init_data.proc_cpuset[wrank],next_obj->cpuset)){
	  is_bound = 1;
	  break;
	}       
      } while((next_obj = hwloc_get_next_obj_by_depth(init_data.hwloc_topology,level,next_obj)));

      mpi_error = MPI_Info_set(*hw_info,str,is_bound ? "true" : "false");
      if(mpi_error != MPI_SUCCESS) return mpi_error;
    }

    // Add NUMANodes
    numa_obj = hwloc_get_obj_by_type(init_data.hwloc_topology,HWLOC_OBJ_NUMANODE,0);
    do{
      if(hwloc_bitmap_isincluded(init_data.proc_cpuset[wrank],numa_obj->cpuset)){
	is_bound = 1;
	break;
      }       
    } while((numa_obj = hwloc_get_next_obj_by_type(init_data.hwloc_topology,HWLOC_OBJ_NUMANODE,numa_obj)));

    mpi_error = MPI_Info_set(*hw_info,"hwloc://NUMANode",is_bound ? "true" : "false");
    if(mpi_error != MPI_SUCCESS) return mpi_error;
    
  }
  return mpi_error;
}

int MPIX_Get_hw_resource_types(MPI_Info *info)
{
  hwloc_obj_t base_obj;
  int mpi_error = MPI_SUCCESS;
  int nlevels = 0;
  int nlevels_node = 0;
  int current_level = 0;
  int wrank;
  char str[STR_SIZE];
  char str2[STR_SIZE];  
  
  mpi_error = MPI_Info_create(info);
  if(mpi_error != MPI_SUCCESS) return mpi_error;

  mpi_error =  MPI_Comm_rank(init_data.ref_comm,&wrank);
  if(mpi_error != MPI_SUCCESS) return mpi_error;
  
  /* refresh binding */ 
  mpi_error = hwloc_get_proc_cpubind(init_data.hwloc_topology,
				     init_data.proc_data[wrank].pid,
				     init_data.proc_cpuset[wrank],
				     HWLOC_CPUBIND_PROCESS);      
  if (-1 == mpi_error) return MPI_ERR_INTERN;
  
  nlevels_node = hwloc_topology_get_depth(init_data.hwloc_topology);  
  nlevels = nlevels_node;
#ifdef NETLOC_SUPPORT
  nlevels += init_data.my_net.ndims;
#endif /*NETLOC_SUPPORT*/
  sprintf(str,"%i",nlevels);
  mpi_error = MPI_Info_set(*info,"mpi_hw_res_nresources",str);
  if(mpi_error != MPI_SUCCESS) return mpi_error;

#ifdef NETLOC_SUPPORT  
#ifdef DEBUG
  fprintf(stdout,"==============> topo depth is %i (net %i, node %i)\n ",nlevels,init_data.my_net.ndims,nlevels_node );
#endif
  for(current_level = 0 ; current_level < init_data.my_net.ndims ; current_level++)
    {
      memset(str,0,STR_SIZE);
      sprintf(str,"mpi_hw_res_%i_name",current_level);
      memset(str2,0,STR_SIZE);
      sprintf(str2,"Net_Level_%i",current_level);
      mpi_error = MPI_Info_set(*info,str,str2);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      /* obviously */
      memset(str,0,STR_SIZE);
      sprintf(str,"mpi_hw_res_%i_valid",current_level);
      mpi_error = MPI_Info_set(*info,str,"true");
      if(mpi_error != MPI_SUCCESS) return mpi_error;
    }
#else
#ifdef DEBUG
  fprintf(stdout,"==============> topo depth is %i (All node)\n ",nlevels);
#endif
#endif /*NETLOC_SUPPORT*/

  /* Important: a level in hwloc is always homogeneous (cousins)  */
  /* if two different resources directly share the same parent */
  /* they will appear in the tree as two different levels */

  /* NUMANodes are not listed in the levels if they are an alias */

  /* handle intranode levels */
  for(base_obj = hwloc_get_root_obj(init_data.hwloc_topology) ;
      base_obj != NULL ;
      base_obj = base_obj->first_child, current_level++)
    {
      int num_aliases_up = 0;
      int num_aliases_down = 0;
      int is_bound = 0;
      hwloc_obj_t restore_obj = base_obj;
      hwloc_obj_t next_obj;
      int idx = 0;
      
      memset(str,0,STR_SIZE);
      sprintf(str,"mpi_hw_res_%i_name",current_level);
      mpi_error = MPI_Info_set(*info,str,hwloc_obj_type_string(base_obj->type));
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      
      memset(str,0,STR_SIZE);
      sprintf(str,"mpi_hw_res_%i_valid",current_level);
      next_obj = base_obj;
      while(next_obj){
	if(hwloc_bitmap_isincluded(init_data.proc_cpuset[wrank],next_obj->cpuset)){
	  is_bound = 1;
	  break;
	}       
	next_obj = hwloc_get_next_obj_by_type(init_data.hwloc_topology,base_obj->type,next_obj);
	idx++;		
      }
      mpi_error = MPI_Info_set(*info,str,is_bound ? "true" : "false");
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      	
      /* handle aliases, up and down */
      /* up */
      while( (base_obj = base_obj->parent) && (1 == base_obj->arity))
	num_aliases_up++;
#ifdef DEBUG
      fprintf(stdout,"Num aliases up for res : %i\n",num_aliases_up);
#endif
      for(idx = 0 ; idx < num_aliases_up ; idx++)
	{
	  memset(str,0,STR_SIZE);
	  sprintf(str,"mpi_hw_res_%i_alias_%i",current_level,idx);
	  memset(str2,0,STR_SIZE);
	  sprintf(str2,"%i",current_level - num_aliases_up + idx);
	  mpi_error = MPI_Info_set(*info,str,str2);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	  memset(str,0,STR_SIZE);
	  sprintf(str,"mpi_hw_res_%i_valid",current_level - num_aliases_up + idx);
	  mpi_error = MPI_Info_set(*info,str,is_bound ? "true" : "false");
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	}         
      /* back to original obj */
      base_obj = restore_obj;
      /* down */
      while((1 == base_obj->arity) && (base_obj = base_obj->first_child))
	num_aliases_down++;
#ifdef DEBUG
      fprintf(stdout,"Num aliases down for res %i : %i\n",current_level, num_aliases_down);
#endif
      for(idx = 0 ; idx < num_aliases_down ; idx++)
	{
	  memset(str,0,STR_SIZE);
	  sprintf(str,"mpi_hw_res_%i_alias_%i",current_level,idx+num_aliases_up);
	  memset(str2,0,STR_SIZE);
	  sprintf(str2,"%i",current_level + 1 + idx);
	  mpi_error = MPI_Info_set(*info,str,str2);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	  memset(str,0,STR_SIZE);
	  sprintf(str,"mpi_hw_res_%i_valid",current_level + 1 + idx);
	  mpi_error = MPI_Info_set(*info,str,is_bound ? "true" : "false");
	  if(mpi_error != MPI_SUCCESS) return mpi_error;	  
	}    
      /* back to original obj */
      base_obj = restore_obj;
      
      memset(str,0,STR_SIZE);
      sprintf(str,"mpi_hw_res_%i_naliases", current_level);
      memset(str2,0,STR_SIZE);
      sprintf(str2,"%i",num_aliases_down + num_aliases_up);
      mpi_error = MPI_Info_set(*info,str,str2);
      if(mpi_error != MPI_SUCCESS) return mpi_error;    
    }
  
  return mpi_error;
}

/****************************************************************************/
/*                                 UTILITIES                                */
/****************************************************************************/
int MPIX_Comm_hsplit_with_roots(MPI_Comm comm, int split_type, MPI_Info info, MPI_Comm *newcomm, MPI_Comm *root_comm)
{
  hwloc_obj_type_t obj_type;
  hwloc_obj_t base_obj;
  char *str = NULL;
  int wrank; 
  int rank;
  int newrank;
  int numprocs;
  int keylen = 0;
  int flag = 0;
  int root_color = MPI_UNDEFINED;
  int mpi_error = MPI_SUCCESS;
  
  mpi_error = MPI_Comm_rank(init_data.ref_comm,&wrank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;
  
  mpi_error = MPI_Comm_rank(comm,&rank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;
  
  mpi_error = MPI_Comm_size(comm,&numprocs);
  if (mpi_error != MPI_SUCCESS) return mpi_error;
  
  if(split_type == MPI_COMM_TYPE_HW_GUIDED)
    {
      if(info == MPI_INFO_NULL)
	{
	  *newcomm = MPI_COMM_NULL;
	  return MPI_SUCCESS;	      
	}
      else
	{
	  mpi_error = MPI_Info_get_valuelen(info,"mpi_hw_resource_type",&keylen,&flag);
	  if (mpi_error != MPI_SUCCESS) return mpi_error;
	  
	  if(!flag)
	    {
	      *newcomm = MPI_COMM_NULL;
	      return MPI_SUCCESS;	      
	    }
	  else
	    {
	      int split_color = 0;
	      str = calloc(keylen+1,sizeof(char));          
	      mpi_error = MPI_Info_get(info,"mpi_hw_resource_type",keylen,str,&flag);
	      if (mpi_error != MPI_SUCCESS) return mpi_error;
#ifdef DEBUG
	      fprintf(stdout,"====================> Got info val : %s | %i\n",str, mpi_error);
#endif
	      /* First, deal with type_shared */
	      /* Select nodes but this is not always the right answer */
	      /* depending on the network, newcomm could be a *duplicate* of comm */
	      if(strcmp(str,"mpi_shared_memory") == 0)
		{
		  int all_local;
			     
		  mpi_error = all_local_procs(comm,&all_local,&split_color);
		  if (mpi_error != MPI_SUCCESS) return mpi_error;
		  
		  mpi_error = MPI_Comm_split(comm,split_color,rank,newcomm);
		  if (mpi_error != MPI_SUCCESS) return mpi_error;
		  
		  if(*newcomm != MPI_COMM_NULL){
		    mpi_error = MPI_Info_set(info,"mpi_hw_resource_type","Machine");

		    /* color for the roots comms */
		    mpi_error = MPI_Comm_rank(*newcomm,&newrank);
		    if(mpi_error != MPI_SUCCESS) return mpi_error;
		    if(0 == newrank)
		      root_color = 1;
		  }
		}
	      else 
#ifdef NETLOC_SUPPORT
		/* Deal with network split */
		if((strncmp(str,"Net_level_",strlen("Net_level_")) == 0) ||
		   (strncmp(str,"Net_Level_",strlen("Net_Level_")) == 0) ||
		   (strncmp(str,"NET_LEVEL_",strlen("NET_LEVEL_")) == 0))
		  {
		    int level  = str[strlen("Net_Level_")]-'0';
		    
		    split_color = init_data.my_net.coords[level];
#ifdef DEBUG
		    fprintf(stdout,"================================= split at net level : %i (color : %i)\n",level,split_color);
#endif
		    mpi_error = MPI_Comm_split(comm,split_color,rank,newcomm);

		    /* color for the roots comms */
		    mpi_error = MPI_Comm_rank(*newcomm,&newrank);
		    if(mpi_error != MPI_SUCCESS) return mpi_error;
		    if(0 == newrank)
		      root_color = 1;				       
		  }   
		else 
#endif /* NETLOC_SUPPORT */
		  {
		    /* Split inside node */		      
		    /* FIXME: need to reimplement sscanf to strictly comply to spec */
		    mpi_error = hwloc_type_sscanf(str,&obj_type,NULL,0);
		    
		    if (0 == mpi_error) {
		      mpi_error = hwloc_get_proc_cpubind(init_data.hwloc_topology,
							 init_data.proc_data[wrank].pid,
							 init_data.proc_cpuset[wrank],
							 HWLOC_CPUBIND_PROCESS);
		      if (-1 == mpi_error) return MPI_ERR_INTERN;
		      
		      base_obj = hwloc_get_next_obj_covering_cpuset_by_type(init_data.hwloc_topology,
									    init_data.proc_cpuset[wrank],
									    obj_type,
									    NULL);
		      if(base_obj != NULL)
			{
			  int all_local;
			  int color;
			  unsigned int numbits = 0;
			  unsigned int split_color = 0;
			  int higherbits = 0;
			  
			  /* we know locality, we just need the color */
			  mpi_error = all_local_procs(comm,&all_local,&color);
			  if (mpi_error != MPI_SUCCESS) return mpi_error;
			  
			  for (numbits = 0, higherbits = numprocs-1  ; higherbits ; higherbits >>= 1)
			    numbits++;			      
			  split_color  = base_obj->logical_index << numbits;
			  split_color |= (unsigned int)color;

			  if ((split_color & (0x1 << ((sizeof(int)*8)-1))) >> ((sizeof(int)*8)-1)){
#ifdef DEBUG
			    fprintf(stdout,"=========================================> Error in color : real %u (in split %i)\n",split_color,split_color);
#endif
			    *newcomm = MPI_COMM_NULL;
			    return MPI_ERR_INTERN;	       
			  }
			  
#ifdef DEBUG
			  fprintf(stdout,"Found obj number %i | color is %i\n",base_obj->logical_index,color);
			  fprintf(stdout,"===========================================> Split_color is %i\n",split_color);
#endif			      
			  mpi_error = MPI_Comm_split(comm,(int)split_color,rank,newcomm); 
			  if (mpi_error != MPI_SUCCESS) return mpi_error;			      
			  
			  /* get the *real* resource name */
			  if(*newcomm != MPI_COMM_NULL) {
			    if(info != MPI_INFO_NULL) {
			      char str[STR_SIZE];
			      
			      hwloc_obj_type_snprintf(str,STR_SIZE,base_obj,0);
			      mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
			      if(mpi_error != MPI_SUCCESS) return mpi_error;
			    }
			    /* color for the roots comms */
			    mpi_error = MPI_Comm_rank(*newcomm,&newrank);
			    if(mpi_error != MPI_SUCCESS) return mpi_error;
			    if(0 == newrank)
			      root_color = base_obj->logical_index;
			  }
			} else {
			/* Specified type is unknown */
			*newcomm = MPI_COMM_NULL;
			return MPI_SUCCESS;	      
		      }  
		    } else {
		      /* Specified type is unknown */
		      *newcomm = MPI_COMM_NULL;
		      return MPI_SUCCESS;	      
		    }
		  } /* net split */
	    } /* flag == true => keyval found  */
	} /* info !=  NULL */
    }
  else if (split_type == MPI_COMM_TYPE_HW_UNGUIDED)
    {
      int all_local;
      int color;
#ifdef NETLOC_SUPPORT
      int level;
      /* Split on the first different level in switches */
      level = get_net_level(comm,rank);
      if (level == -1) return mpi_error;
      
      if(level < init_data.my_net.ndims) {
	/* processes are on different switches, split at this level*/
	color = init_data.my_net.coords[level];
	
	mpi_error = MPI_Comm_split(comm,color,rank,newcomm); 
	if (mpi_error != MPI_SUCCESS) return mpi_error;
	
	if(*newcomm != MPI_COMM_NULL){
	  if (info != MPI_INFO_NULL){
	    char str[STR_SIZE];
	    memset(str,0,STR_SIZE);
	    mpi_error = sprintf(str,"Net_level_%i",level);
	    if(mpi_error < 0) return MPI_ERR_INTERN;
	    
	    mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
	    if(mpi_error != MPI_SUCCESS) return mpi_error;
	  }
	  /* color for the roots comms */
	  mpi_error = MPI_Comm_rank(*newcomm,&newrank);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	  if(0 == newrank)
	    root_color = 1;	  
	}
      } else {
	/* all processes are on the same switch but are they  */
	/* on the same node? */
	mpi_error = all_local_procs(comm,&all_local,&color);
	if (mpi_error != MPI_SUCCESS) return mpi_error;
      }     
#else /* NETLOC_SUPPORT */
      mpi_error = all_local_procs(comm,&all_local,&color);
      if (mpi_error != MPI_SUCCESS) return mpi_error;
#endif /* NETLOC_SUPPORT */
      
      if (!all_local){
	/* this creates as many comms as nodes */
	mpi_error = MPI_Comm_split(comm,color,rank,newcomm); 
	if (mpi_error != MPI_SUCCESS) return mpi_error;
	
	if(*newcomm != MPI_COMM_NULL) {	  
	  if (info != MPI_INFO_NULL){
	    mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",hwloc_obj_type_string(get_obj(*newcomm)->type));
	    if(mpi_error != MPI_SUCCESS) return mpi_error;
	  }
	  /* color for the roots comms */
	  mpi_error = MPI_Comm_rank(*newcomm,&newrank);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	  if(0 == newrank)
	    root_color = 1;
	}
      } else {
	/* All processes are local */
	hwloc_obj_t res_obj;
	hwloc_obj_t base_obj;
	int color = MPI_UNDEFINED;
	int wrank;
	unsigned int idx;
	
	res_obj = get_obj(comm);
	if(!res_obj) return MPI_ERR_INTERN;

	base_obj = res_obj;
	
	mpi_error = MPI_Comm_rank(init_data.ref_comm,&wrank);
	if (mpi_error != MPI_SUCCESS) return mpi_error;
	
	for(idx = 0; idx < res_obj->arity ; idx++)
	  if(hwloc_bitmap_isincluded(init_data.proc_cpuset[wrank],
				     res_obj->children[idx]->cpuset)){
	    color = idx;
	    break;
	  }
	
	/* we take the first child, go deeper until redundancies are handled */      
	if(color != MPI_UNDEFINED){
	  res_obj = res_obj->children[color];
	  while ((res_obj->arity == 1) && 
		     (hwloc_bitmap_isequal(res_obj->cpuset,res_obj->first_child->cpuset)))
	    res_obj = res_obj->first_child;
	}
	
	/* if color is undefined, newcomm is MPI_COMM_NULL */
	mpi_error = MPI_Comm_split(comm,color,rank,newcomm);      
	if(*newcomm != MPI_COMM_NULL){	  
	  if (info != MPI_INFO_NULL){
	    char str[STR_SIZE];
	    /* then color is not MPI_UNDEFINED */
	    hwloc_obj_type_snprintf(str,STR_SIZE,res_obj,0);
	    mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
	    if(mpi_error != MPI_SUCCESS) return mpi_error;
	  }
	  /* color for the roots comms */
	  /* use base obj and not res obj ! */
	  mpi_error = MPI_Comm_rank(*newcomm,&newrank);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	  if(0 == newrank)
	    root_color = base_obj->logical_index;
	}	  
      } /* all local */
    } /* unguided */
#ifdef NETLOC_SUPPORT
    next:
#endif
  /* creation of roots comm for this level */
  /* if color is UNDEFINED, then root_comm is COMM_NULL */
  mpi_error = MPI_Comm_split(comm,root_color,rank,root_comm);
  if(mpi_error != MPI_SUCCESS) return mpi_error;
 
  if((*newcomm != MPI_COMM_NULL) && (info != MPI_INFO_NULL)){
    MPI_Group g1,g2;
    int newrank = -1;
    int is_master = 0;
    int subcomms_num = 0;
    int subcomm_rank = 0;
    int in_rank = 0 ;
    int out_rank;
    int *values = NULL;
    int size;	  
    int idx;
    char str[STR_SIZE];
    
    mpi_error = MPI_Comm_rank(*newcomm,&newrank);
    if(mpi_error != MPI_SUCCESS) return mpi_error;
    
    if(!newrank)
      is_master++;
    
    mpi_error = MPI_Comm_size(comm,&size);	  
    if(mpi_error != MPI_SUCCESS) return mpi_error;
    
    values = (int *)malloc(sizeof(int)*size);
    if(!values) return MPI_ERR_INTERN;
    
    values = memset(values,0,sizeof(int)*size);
    if(!values) return MPI_ERR_INTERN;
    
    values[rank] = is_master;
    
    mpi_error = MPI_Allgather(MPI_IN_PLACE,0,MPI_DATATYPE_NULL,values,1,MPI_INT,comm);
    if(mpi_error != MPI_SUCCESS) return mpi_error;
    
    /* to get the number of subcomms */
    /* count the number of masters (ie rank 0) */
    subcomms_num = 0;
    for(idx = 0; idx < size ; idx++)
      subcomms_num += values[idx];
    memset(str,0,STR_SIZE);
    mpi_error = sprintf(str,"%i",subcomms_num);
    if(mpi_error < 0) return MPI_ERR_INTERN;
    
    mpi_error = MPI_Info_set(info,"mpi_hw_resource_num",str);
    if(mpi_error != MPI_SUCCESS) return mpi_error;
    /* the rank of the subcomm is determined by the */
    /* rank of its master in the old comm           */
    mpi_error = MPI_Comm_group(comm,&g1);
    if(mpi_error != MPI_SUCCESS) return mpi_error;
    
    mpi_error = MPI_Comm_group(*newcomm,&g2);
    if(mpi_error != MPI_SUCCESS) return mpi_error;
    /* in_rank is 0 : master */
    mpi_error = MPI_Group_translate_ranks(g2,1,&in_rank,g1,&out_rank);
    if(mpi_error != MPI_SUCCESS) return mpi_error;
    
    for(idx = 0; idx < out_rank ; idx++)
      subcomm_rank += values[idx];
    memset(str,0,STR_SIZE);
    mpi_error = sprintf(str,"%i",subcomm_rank);
    if(mpi_error < 0) return MPI_ERR_INTERN;
    
    mpi_error = MPI_Info_set(info,"mpi_hw_resource_index",str);
    if(mpi_error != MPI_SUCCESS) return mpi_error;
    free(values);
  }
  
  return mpi_error;	
}  

int MPIX_Comm_hsplit_with_global_roots(MPI_Comm comm, int split_type,MPI_Info info, MPI_Comm *newcomm, MPI_Comm *root_comm)
{
  hwloc_obj_type_t obj_type;
  hwloc_obj_t base_obj;
  char *str = NULL;
  int wrank; 
  int rank;
  int newrank;
  int numprocs;
  int keylen = 0;
  int flag = 0;
  int root_color = MPI_UNDEFINED;
  int mpi_error = MPI_SUCCESS;
  
  mpi_error = MPI_Comm_rank(init_data.ref_comm,&wrank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;
  
  mpi_error = MPI_Comm_rank(comm,&rank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;
  
  mpi_error = MPI_Comm_size(comm,&numprocs);
  if (mpi_error != MPI_SUCCESS) return mpi_error;
  
  if(split_type == MPI_COMM_TYPE_HW_GUIDED)
    {
      if(info == MPI_INFO_NULL)
	{
	  *newcomm = MPI_COMM_NULL;
	  return MPI_SUCCESS;	      
	}
      else
	{
	  mpi_error = MPI_Info_get_valuelen(info,"mpi_hw_resource_type",&keylen,&flag);
	  if (mpi_error != MPI_SUCCESS) return mpi_error;
	  
	  if(!flag)
	    {
	      *newcomm = MPI_COMM_NULL;
	      return MPI_SUCCESS;	      
	    }
	  else
	    {
	      int split_color = 0;
	      str = calloc(keylen+1,sizeof(char));          
	      mpi_error = MPI_Info_get(info,"mpi_hw_resource_type",keylen,str,&flag);
	      if (mpi_error != MPI_SUCCESS) return mpi_error;
#ifdef DEBUG
	      fprintf(stdout,"====================> Got info val : %s | %i\n",str, mpi_error);
#endif
	      /* First, deal with type_shared */
	      /* Select nodes but this is not always the right answer */
	      /* depending on the network, newcomm could be a *duplicate* of comm */
	      if(strcmp(str,"mpi_shared_memory") == 0)
		{
		  int all_local;
			     
		  mpi_error = all_local_procs(comm,&all_local,&split_color);
		  if (mpi_error != MPI_SUCCESS) return mpi_error;
		  
		  mpi_error = MPI_Comm_split(comm,split_color,rank,newcomm);
		  if (mpi_error != MPI_SUCCESS) return mpi_error;
		  
		  if(*newcomm != MPI_COMM_NULL){
		    mpi_error = MPI_Info_set(info,"mpi_hw_resource_type","Machine");

		    /* color for the roots comms */
		    mpi_error = MPI_Comm_rank(*newcomm,&newrank);
		    if(mpi_error != MPI_SUCCESS) return mpi_error;
		    if(0 == newrank)
		      root_color = 1;
		  }
		}
	      else 
#ifdef NETLOC_SUPPORT
		/* Deal with network split */
		if((strncmp(str,"Net_level_",strlen("Net_level_")) == 0) ||
		   (strncmp(str,"Net_Level_",strlen("Net_Level_")) == 0) ||
		   (strncmp(str,"NET_LEVEL_",strlen("NET_LEVEL_")) == 0))
		  {
		    int level  = str[strlen("Net_Level_")]-'0';
		    
		    split_color = init_data.my_net.coords[level];
#ifdef DEBUG
		    fprintf(stdout,"================================= split at net level : %i (color : %i)\n",level,split_color);
#endif
		    mpi_error = MPI_Comm_split(comm,split_color,rank,newcomm);

		    /* color for the roots comms */
		    mpi_error = MPI_Comm_rank(*newcomm,&newrank);
		    if(mpi_error != MPI_SUCCESS) return mpi_error;
		    if(0 == newrank)
		      root_color = 1;				       
		    goto next;
		  }   
		else 
#endif /* NETLOC_SUPPORT */
		  {
		    /* Split inside node */		      
		    /* FIXME: need to reimplement sscanf to strictly comply to spec */
		    mpi_error = hwloc_type_sscanf(str,&obj_type,NULL,0);
		    
		    if (0 == mpi_error) {
		      mpi_error = hwloc_get_proc_cpubind(init_data.hwloc_topology,
							 init_data.proc_data[wrank].pid,
							 init_data.proc_cpuset[wrank],
							 HWLOC_CPUBIND_PROCESS);
		      if (-1 == mpi_error) return MPI_ERR_INTERN;
		      
		      base_obj = hwloc_get_next_obj_covering_cpuset_by_type(init_data.hwloc_topology,
									    init_data.proc_cpuset[wrank],
									    obj_type,
									    NULL);
		      if(base_obj != NULL)
			{
			  int all_local;
			  int color;
			  unsigned int numbits = 0;
			  unsigned int split_color = 0;
			  int higherbits = 0;
			  
			  /* we know locality, we just need the color */
			  mpi_error = all_local_procs(comm,&all_local,&color);
			  if (mpi_error != MPI_SUCCESS) return mpi_error;
			  
			  for (numbits = 0, higherbits = numprocs-1  ; higherbits ; higherbits >>= 1)
			    numbits++;			      
			  split_color  = base_obj->logical_index << numbits;
			  split_color |= (unsigned int)color;
			  if ((split_color & (0x1 << ((sizeof(int)*8)-1))) >> ((sizeof(int)*8)-1)){
#ifdef DEBUG
			    fprintf(stdout,"=========================================> Error in color : real %u (in split %i)\n",split_color,split_color);
#endif
			    *newcomm = MPI_COMM_NULL;
			    return MPI_ERR_INTERN;	       
			  }
			  
#ifdef DEBUG
			  fprintf(stdout,"Found obj number %i | color is %i\n",base_obj->logical_index,color);
			  fprintf(stdout,"===========================================> Split_color is %i\n",split_color);
#endif			      
			  mpi_error = MPI_Comm_split(comm,(int)split_color,rank,newcomm); 
			  if (mpi_error != MPI_SUCCESS) return mpi_error;			      
			  
			  /* get the *real* resource name */
			  if(*newcomm != MPI_COMM_NULL) {
			    char str[STR_SIZE];
			    
			    hwloc_obj_type_snprintf(str,STR_SIZE,base_obj,0);
			    mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
			    if(mpi_error != MPI_SUCCESS) return mpi_error;

			    /* color for the roots comms */
			    mpi_error = MPI_Comm_rank(*newcomm,&newrank);
			    if(mpi_error != MPI_SUCCESS) return mpi_error;
			    if(0 == newrank)
			      root_color = 1;
			  }
			} else {
			/* Specified type is unknown */
			*newcomm = MPI_COMM_NULL;
			return MPI_SUCCESS;	      
		      }  
		    } else {
		      /* Specified type is unknown */
		      *newcomm = MPI_COMM_NULL;
		      return MPI_SUCCESS;	      
		    }
		  }
	    } /* flag == true => keyval found  */
	} /* info !=  NULL */
    }
  else if (split_type == MPI_COMM_TYPE_HW_UNGUIDED)
    {
      int all_local;
      int color;
#ifdef NETLOC_SUPPORT
      int level;
      /* Split on the first different level in switches */
      level = get_net_level(comm,rank);
      if (level == -1) return mpi_error;
      
      if(level < init_data.my_net.ndims) {
	/* processes are on different switches, split at this level*/
	color = init_data.my_net.coords[level];
	
	mpi_error = MPI_Comm_split(comm,color,rank,newcomm); 
	if (mpi_error != MPI_SUCCESS) return mpi_error;
	
	if(*newcomm != MPI_COMM_NULL){
	  if (info != MPI_INFO_NULL){
	    char str[STR_SIZE];
	    memset(str,0,STR_SIZE);
	    mpi_error = sprintf(str,"Net_level_%i",level);
	    if(mpi_error < 0) return MPI_ERR_INTERN;
	    
	    mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
	    if(mpi_error != MPI_SUCCESS) return mpi_error;
	  }
	  /* color for the roots comms */
	  mpi_error = MPI_Comm_rank(*newcomm,&newrank);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	  if(0 == newrank)
	    root_color = 1;	  
	}
      } else {
	/* all processes are on the same switch but are they  */
	/* on the same node? */
	mpi_error = all_local_procs(comm,&all_local,&color);
	if (mpi_error != MPI_SUCCESS) return mpi_error;
      }     
#else /* NETLOC_SUPPORT */
      mpi_error = all_local_procs(comm,&all_local,&color);
      if (mpi_error != MPI_SUCCESS) return mpi_error;
#endif /* NETLOC_SUPPORT */
      
      if (!all_local){
	/* this creates as many comms as nodes */
	mpi_error = MPI_Comm_split(comm,color,rank,newcomm); 
	if (mpi_error != MPI_SUCCESS) return mpi_error;
	
	if(*newcomm != MPI_COMM_NULL){
	  if (info != MPI_INFO_NULL){	  
	    mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",hwloc_obj_type_string(get_obj(*newcomm)->type));
	    if(mpi_error != MPI_SUCCESS) return mpi_error;
	  }
	  
	  /* color for the roots comms */
	  mpi_error = MPI_Comm_rank(*newcomm,&newrank);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	  if(0 == newrank)
	    root_color = 1;
	}
      } else {
	/* All processes are local */
	hwloc_obj_t res_obj;
	int color = MPI_UNDEFINED;
	int wrank;
	unsigned int idx;
	
	res_obj = get_obj(comm);
	if(!res_obj) return MPI_ERR_INTERN;
	
	mpi_error = MPI_Comm_rank(init_data.ref_comm,&wrank);
	if (mpi_error != MPI_SUCCESS) return mpi_error;
	
	for(idx = 0; idx < res_obj->arity ; idx++)
	  if(hwloc_bitmap_isincluded(init_data.proc_cpuset[wrank],
				     res_obj->children[idx]->cpuset)){
	    color = idx;
	    break;
	  }
	
	/* we take the first child, go deeper until redundancies are handled */      
	if(color != MPI_UNDEFINED){
	  res_obj = res_obj->children[color];
	  while ((res_obj->arity == 1) && 
		     (hwloc_bitmap_isequal(res_obj->cpuset,res_obj->first_child->cpuset)))
	    res_obj = res_obj->first_child;
	}
	
	/* if color is undefined, newcomm is MPI_COMM_NULL */
	mpi_error = MPI_Comm_split(comm,color,rank,newcomm);      
	if(*newcomm != MPI_COMM_NULL) {
	  if(info != MPI_INFO_NULL) {
	    char str[STR_SIZE];
	    /* then color is not MPI_UNDEFINED */
	    hwloc_obj_type_snprintf(str,STR_SIZE,res_obj,0);
	    mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
	    if(mpi_error != MPI_SUCCESS) return mpi_error;
	  }
	  /* color for the roots comms */
	  mpi_error = MPI_Comm_rank(*newcomm,&newrank);
	  if(mpi_error != MPI_SUCCESS) return mpi_error;
	  if(0 == newrank)
	    root_color = 1;
	}	  
      } /* all local */
    } /* unguided */
#ifdef NETLOC_SUPPORT
    next:
#endif
  /* creation of roots comm for this level */
  /* if color is UNDEFINED, then root_comm is COMM_NULL */
  mpi_error = MPI_Comm_split(comm,root_color,rank,root_comm);
  if(mpi_error != MPI_SUCCESS) return mpi_error;

  return mpi_error;	
}

/****************************************************************************/
/*                           INTERNAL ROUTINES                              */
/****************************************************************************/
static int *get_wranks(MPI_Comm comm,int numprocs)
{
  MPI_Group gcomm,gworld;
  int *ranks = NULL;
  int *wranks = NULL;
  int mpi_error = MPI_SUCCESS;
  int idx;
  
  mpi_error = MPI_Comm_group(comm,&gcomm);
  if(mpi_error != MPI_SUCCESS) return NULL;
  
  mpi_error = MPI_Comm_group(init_data.ref_comm,&gworld);
  if(mpi_error != MPI_SUCCESS) return NULL;
  
  ranks = (int *)alloca(numprocs*sizeof(int)); 
  wranks = (int *)malloc(numprocs*sizeof(int));
  for(idx = 0 ; idx < numprocs ; idx++)
    ranks[idx] = idx;
  mpi_error = MPI_Group_translate_ranks(gcomm,numprocs,ranks,gworld,wranks);
  if(mpi_error != MPI_SUCCESS) return NULL;

  return wranks;
}

static int all_local_procs(MPI_Comm comm,int *locality,int *color)
{
  int numprocs;
  int *wranks = NULL;
  int mpi_error = MPI_SUCCESS;
  int idx;
  
  mpi_error = MPI_Comm_size(comm,&numprocs);
  if(mpi_error != MPI_SUCCESS) return mpi_error;
  
  wranks = get_wranks(comm,numprocs);
  if(wranks == NULL) return MPI_ERR_INTERN;

  *locality = 0;
  for(idx = 0 ; idx < numprocs ; idx++)
    *locality += init_data.node_locality[wranks[idx]];
  
  if(*locality == numprocs)
    {
      *locality = 1;    
      *color = 0; 
    } 
  else 
    {    
      int myrank;
      
      MPI_Comm_rank(comm,&myrank);
      
      *locality = 0;
      *color = idx = 0;
      while((0 != strcmp(init_data.proc_data[wranks[myrank]].node_name,
			 init_data.proc_data[wranks[idx]].node_name)) && (idx < numprocs)){
	(*color)++;
	idx++;
      }
    }
  free(wranks);
  return mpi_error;
}

#ifdef NETLOC_SUPPORT
static int get_net_level(MPI_Comm comm,int rank)
{
  int *wranks = NULL;
  int level;
  int numprocs;
  int mpi_error;  
  
  mpi_error = MPI_Comm_size(comm,&numprocs);
  if (mpi_error != MPI_SUCCESS) return -1;
  
  wranks = get_wranks(comm,numprocs);
  
  for(level  = 0 ; level < init_data.my_net.ndims ; level++) {
    int idx = 0;      
    while ((idx < numprocs) && (((init_data.net_coords + wranks[idx]*init_data.my_net.ndims)[level]) == 
				((init_data.net_coords + wranks[rank]*init_data.my_net.ndims)[level]))){
      idx++;
    }
    if(idx != numprocs)
      break;
    //else
    //  level = init_data.my_net.ndims + 1;
  }

  return level;
}
#endif /* NETLOC_SUPPORT */



static hwloc_obj_t get_obj(MPI_Comm comm)
{
  hwloc_cpuset_t res_set;
  hwloc_obj_t res_obj;
  int numprocs;
  int *wranks = NULL;
  int mpi_error = MPI_SUCCESS;
  int idx;

  mpi_error = MPI_Comm_size(comm,&numprocs);
  if(mpi_error != MPI_SUCCESS) return NULL;
  
  wranks = get_wranks(comm,numprocs);
  if(wranks == NULL) return NULL;  

  /* refresh binding */
  for(idx = 0 ; idx < numprocs ; idx++){
    if(init_data.node_locality[wranks[idx]]){
      mpi_error = hwloc_get_proc_cpubind(init_data.hwloc_topology,
                                         init_data.proc_data[wranks[idx]].pid,
                                         init_data.proc_cpuset[wranks[idx]],
                                         HWLOC_CPUBIND_PROCESS);      
      if (-1 == mpi_error) return NULL;
    }
  }
  
  res_set = hwloc_bitmap_alloc();
  if(!res_set) return NULL;
  for(idx = 0 ; idx < numprocs ; idx++)
    if(init_data.node_locality[wranks[idx]])
      hwloc_bitmap_or(res_set,init_data.proc_cpuset[wranks[idx]],res_set);
  
  res_obj = hwloc_get_obj_covering_cpuset(init_data.hwloc_topology, res_set);
  
#ifdef DEBUG
  {
    char str[STR_SIZE];
    fprintf(stdout,"====== cpuset  :");
    for(idx = 0 ; idx < numprocs ; idx++){
      if(init_data.node_locality[wranks[idx]]){
	hwloc_bitmap_snprintf(str,STR_SIZE,init_data.proc_cpuset[wranks[idx]]);    
	fprintf(stdout," [comm : %i | comm_world : %i | %s] ",idx,wranks[idx],str);
      }
    }    
    fprintf(stdout,"\n");    
    fprintf(stdout,"====== res set  :");
    hwloc_bitmap_snprintf(str,STR_SIZE,res_set);    
    fprintf(stdout,"  %s \n",str);
  }
#endif
  
  hwloc_bitmap_free(res_set);
  free(wranks);
  
  return res_obj;
}


void shmem_cleanup(void)
{
  if(init_data.shmem_data.length){
#ifdef DEBUG
    int ret =
#endif
      shm_unlink(init_data.shmem_data.name);
#ifdef DEBUG
    if ((ret == -1) && (errno == ENOENT))
      fprintf(stdout,"==============> Shmem obj %s already removed\n",init_data.shmem_data.name);
    else
      fprintf(stdout,"==============> Removing shmem obj %s\n",init_data.shmem_data.name);
#endif
  }
}

void shmem_handler(int sig)
{  
  shmem_cleanup();
  if((SIGSEGV == sig) || (SIGFPE == sig))
    abort();
}

/****************************************************************************/
/*                       FIND HOLE for SHMEM                                */
/*       Code adapted from orte/mca/rtc/hwloc/rtc_hwloc.c                   */
/*       Copyright © Inria 2017-2018.  All rights reserved.                 */
/*              For questions : brice.goglin@inria.fr                       */    
/****************************************************************************/
static int parse_map_line(const char *line,
                          unsigned long *beginp,
                          unsigned long *endp,
                          hwloc_vm_map_kind_t *kindp)
{
  const char *tmp = line, *next = NULL;
  unsigned long value = 0;
  
  /* "beginaddr-endaddr " */
  value = strtoull(tmp, (char **) &next, 16);
  if (next == tmp) {
    return MPI_ERR_INTERN;
  }
  
  *beginp = (unsigned long) value;
  
  if (*next != '-') {
    return MPI_ERR_INTERN;
  }
  
  tmp = next + 1;
  value = strtoull(tmp, (char **) &next, 16);
  if (next == tmp) {
    return MPI_ERR_INTERN;
  }
  *endp = (unsigned long) value;
  tmp = next;
  
  if (*next != ' ') {
    return MPI_ERR_INTERN;
  }
  tmp = next + 1;
    
  /* look for ending absolute path */
  next = strchr(tmp, '/');
  if (next) {
    *kindp = VM_MAP_FILE;
  } else {
    /* look for ending special tag [foo] */
    next = strchr(tmp, '[');
    if (next) {
      if (!strncmp(next, "[heap]", 6)) {
	*kindp = VM_MAP_HEAP;
      } else if (!strncmp(next, "[stack]", 7)) {
	*kindp = VM_MAP_STACK;
      } else {
	char *end = NULL;
	if ((end = strchr(next, '\n')) != NULL) {
	  *end = '\0';
	}
#ifdef DEBUG
	fprintf(stdout,"Found special VMA \"%s\" before stack", next);
#endif
	*kindp = VM_MAP_OTHER;
      }
    } else {
      *kindp = VM_MAP_ANONYMOUS;
    }
  }
  
  return MPI_SUCCESS;
}

#define ALIGN2MB (2*1024*1024UL)

static int use_hole(unsigned long holebegin,
                    unsigned long holesize,
                    unsigned long *addrp,
                    unsigned long size)
{
  unsigned long aligned;
  unsigned long middle = holebegin+(holesize/2);

#ifdef DEBUG
  fprintf(stdout,"looking in hole [0x%lx-0x%lx] size %lu (%lu MB) for %lu (%lu MB)\n",
	  holebegin, holebegin+holesize, holesize, holesize>>20, size, size>>20);
#endif
  
    if (holesize < size) {
      return MPI_ERR_INTERN;
    }
    
    /* try to align the middle of the hole on 64MB for POWER's 64k-page PMD */
    #define ALIGN64MB (64*1024*1024UL)
    aligned = (middle + ALIGN64MB) & ~(ALIGN64MB-1);
    if (aligned + size <= holebegin + holesize) {
#ifdef DEBUG      
      fprintf(stdout,"aligned [0x%lx-0x%lx] (middle 0x%lx) to 0x%lx for 64MB\n",
	      holebegin, holebegin+holesize, middle, aligned);
      fprintf(stdout," there are %lu MB free before and %lu MB free after\n",
	      (aligned-holebegin)>>20, (holebegin+holesize-aligned-size)>>20);
#endif
      *addrp = aligned;
      return MPI_SUCCESS;
    }
    
    /* try to align the middle of the hole on 2MB for x86 PMD */
    aligned = (middle + ALIGN2MB) & ~(ALIGN2MB-1);
    if (aligned + size <= holebegin + holesize) {
#ifdef DEBUG      
      fprintf(stdout,"aligned [0x%lx-0x%lx] (middle 0x%lx) to 0x%lx for 2MB\n",
	      holebegin, holebegin+holesize, middle, aligned);
      fprintf(stdout," there are %lu MB free before and %lu MB free after\n",
	      (aligned-holebegin)>>20, (holebegin+holesize-aligned-size)>>20);
#endif
      *addrp = aligned;
      return MPI_SUCCESS;
    }
    
    /* just use the end of the hole */
    *addrp = holebegin + holesize - size;
#ifdef DEBUG      
    fprintf(stdout,"using the end of hole starting at 0x%lx\n", *addrp);
    fprintf(stdout,"there are %lu MB free before\n", (*addrp-holebegin)>>20);
#endif
    return MPI_SUCCESS;
}

/* HOLE KIND == VM_HOLE_IN_LIBS */
static int find_hole(size_t *addrp, size_t size)
{
  unsigned long biggestbegin = 0;
  unsigned long biggestsize = 0;
  unsigned long prevend = 0;
  hwloc_vm_map_kind_t prevmkind = VM_MAP_OTHER;
  int in_libs = 0;
  FILE *file = NULL;
  char line[STR_SIZE];
  
  file = fopen("/proc/self/maps", "r");
  if (!file) {
    return MPI_ERR_INTERN;
  }
  
  while (fgets(line, sizeof(line), file) != NULL) {
    unsigned long begin = 0, end = 0;
    hwloc_vm_map_kind_t mkind = VM_MAP_OTHER;
    
    if (!parse_map_line(line, &begin, &end, &mkind)) {
#ifdef DEBUG
      fprintf(stdout,"found %s from 0x%lx to 0x%lx\n",
	      mkind == VM_MAP_HEAP ? "HEAP" :
	      mkind == VM_MAP_STACK ? "STACK" :
	      mkind == VM_MAP_OTHER ? "OTHER" :
	      mkind == VM_MAP_FILE ? "FILE" :
	      mkind == VM_MAP_ANONYMOUS ? "ANON" : "unknown",
	      begin, end);
#endif
	/* see if we are between heap and stack */
        if (prevmkind == VM_MAP_HEAP) {
	  in_libs = 1;
	}
	if (mkind == VM_MAP_STACK) {
	  in_libs = 0;
	}
	if (in_libs) {
	  /* we're in libs, consider this entry for searching the biggest hole below */
	  /* fallthrough */
	  if (begin-prevend > biggestsize) {
#ifdef DEBUG
	    fprintf(stdout,"new biggest 0x%lx - 0x%lx = %lu (%lu MB)\n",
		    prevend, begin, begin-prevend, (begin-prevend)>>20);
#endif
	    biggestbegin = prevend;
	    biggestsize  = begin-prevend;
	  }
	}
    }
    
    while (!strchr(line, '\n')) {
      if (!fgets(line, sizeof(line), file)) {
	goto done;
      }
    }
    
    if (mkind == VM_MAP_STACK) {
      /* Don't go beyond the stack. Other VMAs are special (vsyscall, vvar, vdso, etc),                                                                                                   
       * There's no spare room there. And vsyscall is even above the userspace limit.                                                                                                     
       */
      break;
    }
    
    prevend = end;
    prevmkind = mkind;    
  }
  
 done:
  fclose(file);
  return use_hole(biggestbegin, biggestsize, addrp, size);
}

/**********************************************************************/
/*                       To be deprecated                             */
/**********************************************************************/
int MPIX_Comm_hsplit_with_allroots(MPI_Comm comm, MPI_Info info, MPI_Comm *newcomm, MPI_Comm *root_comm)
{
  char str[STR_SIZE];
  int root_color = MPI_UNDEFINED;
  int color = MPI_UNDEFINED;
  int mpi_error = MPI_SUCCESS;
  int all_local;
  int rank,newrank;
#ifdef NETLOC_SUPPORT
  int level;
#endif /* NETLOC_SUPPORT */

  mpi_error = MPI_Comm_rank(comm,&rank);
  if (mpi_error != MPI_SUCCESS) return mpi_error;
  
#ifdef NETLOC_SUPPORT
  /* Split on the first different level in switches */
  level = get_net_level(comm,rank);
  if (level == -1) return mpi_error;

  if(level < init_data.my_net.ndims) {
    /* processes are on different switches, split at this level*/
    color = init_data.my_net.coords[level];

    mpi_error = MPI_Comm_split(comm,color,rank,newcomm);
    if (mpi_error != MPI_SUCCESS) return mpi_error;

    if(*newcomm != MPI_COMM_NULL){
      memset(str,0,STR_SIZE);
      mpi_error = sprintf(str,"Net_level%i",level);
      if(mpi_error < 0) return MPI_ERR_INTERN;

      mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
      if(mpi_error != MPI_SUCCESS) return mpi_error;

      /* color for the  roots comm */
      mpi_error = MPI_Comm_rank(*newcomm,&newrank);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      root_color = newrank;
      goto next;
    }
  } else {
    /* all processes are on the same switch but are they  */
    /* on the same node? */
    mpi_error = all_local_procs(comm,&all_local,&color);
    if (mpi_error != MPI_SUCCESS) return mpi_error;
  }
#else /* NETLOC_SUPPORT */
  mpi_error = all_local_procs(comm,&all_local,&color);
  if (mpi_error != MPI_SUCCESS) return mpi_error;
#endif /* NETLOC_SUPPORT */

  if (!all_local){
    /* this creates as many comms as nodes */
    mpi_error = MPI_Comm_split(comm,color,rank,newcomm);
    if (mpi_error != MPI_SUCCESS) return mpi_error;
    if(*newcomm != MPI_COMM_NULL){
      mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",hwloc_obj_type_string(get_obj(*newcomm)->type));
      if(mpi_error != MPI_SUCCESS) return mpi_error;

      /* color for the roots comm */
      mpi_error = MPI_Comm_rank(*newcomm,&newrank);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      root_color = newrank;
    }
  }
  else {
    /* All processes are local */
    hwloc_obj_t res_obj;
    int color2 = MPI_UNDEFINED;
    int wrank;
    unsigned int idx;
    
    mpi_error = MPI_Comm_rank(MPI_COMM_WORLD,&wrank);
    if (mpi_error != MPI_SUCCESS) return mpi_error;

    res_obj = get_obj(comm);
    if(!res_obj) return MPI_ERR_INTERN;

    for(idx = 0; idx < res_obj->arity ; idx++)
      if(hwloc_bitmap_isincluded(init_data.proc_cpuset[wrank],
				 res_obj->children[idx]->cpuset)){
	color2 = idx;
	break;
      }
    /* we take the first child, go deeper until redundancies are handled */
    if(color2 != MPI_UNDEFINED){
      res_obj = res_obj->children[color2];
      while ((res_obj->arity == 1) &&
	     (hwloc_bitmap_isequal(res_obj->cpuset,res_obj->first_child->cpuset)))
	res_obj = res_obj->first_child;
    }

    /* if color is undefined, newcomm is MPI_COMM_NULL */
    mpi_error = MPI_Comm_split(comm,color2,rank,newcomm);

    if(*newcomm != MPI_COMM_NULL){
      /* then color2 is not MPI_UNDEFINED */
      hwloc_obj_type_snprintf(str,STR_SIZE,res_obj,0);
      mpi_error = MPI_Info_set(info,"mpi_hw_resource_type",str);
      if(mpi_error != MPI_SUCCESS) return mpi_error;

      /* color for the  roots comm */
      mpi_error = MPI_Comm_rank(*newcomm,&newrank);
      if(mpi_error != MPI_SUCCESS) return mpi_error;
      root_color = newrank;
    }
  }
#ifdef NETLOC_SUPPORT
 next:
#endif
  /* creation comm des roots du niveau */
  /* if color is UNDEFINED, then root_comm is COMM_NULL */
  mpi_error = MPI_Comm_split(comm,root_color,rank,root_comm);
  if(mpi_error != MPI_SUCCESS) return mpi_error;

  return mpi_error;
}
