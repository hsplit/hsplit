#define _DEFAULT_SOURCE /* strsep*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <alloca.h>
#include "mpi.h"
#include "hsplit.h"

#define NLEVELS 16

void display_comm_members(MPI_Comm comm, int type)
{
  int size;
  int *ranks = NULL;
  int *wranks = NULL;
  MPI_Group gcomm,gworld;
  int index;

  MPI_Comm_size(comm,&size);
  wranks = alloca(size*sizeof(int));
  ranks = alloca(size*sizeof(int));
  for(index = 0 ; index < size ; index++)
    ranks[index] = index;
  MPI_Comm_group(comm,&gcomm);
  MPI_Comm_group(MPI_COMM_WORLD,&gworld);
  MPI_Group_translate_ranks(gcomm,size,ranks,gworld,wranks);
  if(0 == type)
    fprintf(stdout,"=== Subcomm members (COMM_WORLD ranks) : ");
  else if (1 == type)
    fprintf(stdout,"=== Root comm members (COMM_WORLD ranks) : ");
  for(index = 0 ; index < size ; index++)
    fprintf(stdout," %i ",wranks[index]);
  fprintf(stdout,"\n");
}  

int main(int argc, char **argv)
{
  if(argc < 2)
    {
      fprintf(stdout,"Usage : sample_prog #processes \n");
    }
  else
    {

      MPI_Comm in_comm;
      MPI_Comm out_comm[NLEVELS];
      MPI_Info infos[NLEVELS];
      char *type = NULL;
      int num_procs = atoi(argv[1]);
      int *rank_array = alloca(num_procs*sizeof(int));
      int num;
      int error;
      int rank,wrank;
      int idx;
	  
      for(idx = 0 ; idx < num_procs ; idx++)
	rank_array[idx] = idx; 

      
      MPI_Init(NULL,NULL);
      error = MPIX_Hinit();
      if(error != MPI_SUCCESS)
	fprintf(stdout,"================== Init failed\n");
      
      MPI_Comm_size(MPI_COMM_WORLD,&num);
      MPI_Comm_rank(MPI_COMM_WORLD,&rank);
      wrank = rank;
      
      if(num_procs > num){
	fprintf(stdout,"Invalid Arg : should be at most %i\n",num);
	MPI_Finalize();
	return 0;
      }

      fprintf(stdout,"================== Test 1 : MPIX_Get_hw_resource_min\n");
      {
	error = MPIX_Get_hw_resource_min(MPI_COMM_WORLD,num_procs,rank_array,&type);
	if(error != MPI_SUCCESS)
	  fprintf(stdout,"================> Failure  in MPIX_Get_hw_resource_min : %i\n",error);
	fprintf(stdout,"Name obj is : %s\n",type); 
      }
       
      fprintf(stdout,"================== Test 2 :  MPIX_Comm_split_type w/UNGUIDED \n");
      {
	in_comm = MPI_COMM_WORLD;
	idx = 0;
	while(in_comm != MPI_COMM_NULL)
	  {
	    int in_rank,rank;
#ifdef ROOTS_IFACE
	    MPI_Comm master_comm[NLEVELS];
#endif
	    MPI_Comm_rank(in_comm,&in_rank);
	    MPI_Info_create(&infos[idx]);
	    
#ifdef ROOTS_IFACE
	    //#warning "==================> Using ROOTS IFACE"
	    MPIX_Comm_hsplit_with_roots(in_comm,MPI_COMM_TYPE_HW_UNGUIDED,infos[idx],&out_comm[idx],&master_comm[idx]);
#else
	    //#warning "==================> Using REGULAR IFACE"
	    MPIX_Comm_split_type(in_comm,MPI_COMM_TYPE_HW_UNGUIDED,in_rank,infos[idx],&out_comm[idx]);
#endif	 
	    if (out_comm[idx] != MPI_COMM_NULL){
	      MPI_Comm_rank(out_comm[idx],&rank);
	      if( 0 == rank ) {
		int flag,keylen;
		char *str = NULL;
		fprintf(stdout,"[%i] ========== SubLevel %i info : \n",wrank,idx);
		MPI_Info_get_valuelen(infos[idx],"mpi_hw_resource_type",&keylen,&flag);
		if(flag){
		  str = calloc((keylen+1),sizeof(char));
		  MPI_Info_get(infos[idx],"mpi_hw_resource_type",keylen,str,&flag);
		  fprintf(stdout,"[%i] === Subcomm type %s\n",wrank,flag?str:"unknown");
		  free(str);
		}
		MPI_Info_get_valuelen(infos[idx],"mpi_hw_resource_num",&keylen,&flag);
		if(flag){
		  str = calloc((keylen+1),sizeof(char));
		  MPI_Info_get(infos[idx],"mpi_hw_resource_num",keylen,str,&flag);
		  fprintf(stdout,"[%i] === Subcomm number %s\n",wrank,flag?str:"unknown");
		  free(str);
		}
		MPI_Info_get_valuelen(infos[idx],"mpi_hw_resource_index",&keylen,&flag);
		if(flag){
		  str = calloc((keylen+1),sizeof(char));
		  MPI_Info_get(infos[idx],"mpi_hw_resource_index",keylen,str,&flag);
		  fprintf(stdout,"[%i] === Subcomm index %s\n",wrank,flag?str:"unknown");
		  free(str);
		}		
		display_comm_members(out_comm[idx],0);	      
#ifdef ROOTS_IFACE
		if(master_comm[idx] != MPI_COMM_NULL)// && (0 == in_rank))
		  display_comm_members(master_comm[idx],1);	     
#endif
	      }
	    }
	    in_comm = out_comm[idx];
	    assert(++idx < NLEVELS);		 
	  }
      }
      
      fprintf(stdout,"================== Test 3 : MPIX_Get_hw_domain_neighbours \n");      
      {
	int num_neighbors;
	MPI_Group neighbors,gworld;
	int *ranks = NULL; 
	int *wranks = NULL;
	int i,j;

	for(i = 0 ; i < 4 ; i++){      
	  MPIX_Get_hw_domain_neighbours(MPI_COMM_WORLD,i,0,&neighbors);
	  MPI_Group_size(neighbors,&num_neighbors);

	  fprintf(stdout,"Distance[%i] : ",i);	  
	  fprintf(stdout,"Num neighbors is : %i \n", num_neighbors);

	  wranks = (int *)alloca(num_neighbors*sizeof(int));
	  ranks = (int *)alloca(num_neighbors*sizeof(int));
	  for(j = 0 ; j < num_neighbors ; j++)
	    ranks[j] = j;
	  
	  MPI_Comm_group(MPI_COMM_WORLD,&gworld);	
	  MPI_Group_translate_ranks(neighbors,num_neighbors,ranks,gworld,wranks);

	  for(j = 0 ; j < num_neighbors ; j++)
	    fprintf(stdout,"Neighbour[%i]:%i | ",j,wranks[j]);
	  fprintf(stdout,"\n");	  

	}
      }

      fprintf(stdout,"================== Test 4 : MPIX_Get_hw_topology_info (1)\n");      
      {
	MPI_Info info;
	char str[64];
	char *str2 = NULL;
	int numlevels;
	int flag;
	int idx;
	int keylen = 0;
	
	MPI_Info_create(&info);
	MPIX_Get_hw_topology_info(&numlevels,info);
	
	fprintf(stdout,"============> Number of levels : %i\n",numlevels);

	for(idx = 0 ; idx < numlevels; idx++){
	  sprintf(str,"MPI_HW_LEVEL%d",idx);	  
	  MPI_Info_get_valuelen(info,str,&keylen,&flag);
	  if(flag){
	    str2  = calloc(keylen+1,sizeof(char));	    
	    MPI_Info_get(info,str,keylen,str2,&flag);
	    if (flag)
	      fprintf(stdout,"%s type is %s\n",str,str2);
	    free(str2);
	  }
	}
      }

      fprintf(stdout,"================== Test 5 : testing \"Socket\" resource type\n");      
      {
	MPI_Comm out_comm;
        MPI_Info info;
        int rank;

        MPI_Info_create(&info);
        MPI_Info_set(info,"mpi_hw_resource_type","Socket");
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        MPIX_Comm_split_type(MPI_COMM_WORLD,MPI_COMM_TYPE_HW_GUIDED,rank,info,&out_comm);

        /* check info about subcomm */
        if (out_comm != MPI_COMM_NULL){
	  int flag,keylen;
	  char *str = NULL;
	  fprintf(stdout,"[%i] ========== SubLevel %i info : \n",wrank,idx);
	  MPI_Info_get_valuelen(info,"mpi_hw_resource_type",&keylen,&flag);
	  if(flag){
	    str = calloc((keylen+1),sizeof(char));
	    MPI_Info_get(info,"mpi_hw_resource_type",keylen,str,&flag);
	    fprintf(stdout,"[%i] === Subcomm type %s\n",wrank,flag?str:"unknown");
	    free(str);		    
	  }
	  MPI_Info_get_valuelen(info,"mpi_hw_resource_num",&keylen,&flag);
	  if(flag){
	    str = calloc((keylen+1),sizeof(char));
	    MPI_Info_get(info,"mpi_hw_resource_num",keylen,str,&flag);
	    fprintf(stdout,"[%i] === Subcomm number %s\n",wrank,flag?str:"unknown");
	    free(str);		    
	  }
	  MPI_Info_get_valuelen(info,"mpi_hw_resource_index",&keylen,&flag);
	  if(flag){
	    str = calloc((keylen+1),sizeof(char));
	    MPI_Info_get(info,"mpi_hw_resource_index",keylen,str,&flag);
	    fprintf(stdout,"[%i] === Subcomm index %s\n",wrank,flag?str:"unknown");
	    free(str);
	  }		
        }
        else {
          fprintf(stdout,"No level found\n");
        }
      }

      fprintf(stdout,"================== Test 6 : MPIX_Get_hw_resource_types (2)\n");      
      {
	MPI_Info info;
	int flag,keylen;
	char *str = NULL;
	int num_res = 0;
	int idx;
	
	MPIX_Get_hw_resource_types(&info);
	MPI_Info_get_valuelen(info,"mpi_hw_res_nresources",&keylen,&flag);
	if(flag){
	  str = calloc(keylen+1,sizeof(char));	    
	  MPI_Info_get(info,"mpi_hw_res_nresources",keylen,str,&flag);
	  num_res = atoi(str);
	  fprintf(stdout,"Number of available HW resources : %s (%i)\n",str,num_res);
	  free(str);
	}

	for(idx = 0 ; idx < num_res; idx++)
	  {
	    char str2[64];
	    sprintf(str2,"mpi_hw_res_%i_type",idx);
	    MPI_Info_get_valuelen(info,str2,&keylen,&flag);
	    if(flag){
	      str = calloc(keylen+1,sizeof(char));	    
	      MPI_Info_get(info,str2,keylen,str,&flag);
	      fprintf(stdout,"HW resource %i is : %s\n",idx,str);
	      free(str);
	    }
	    
	    sprintf(str2,"mpi_hw_res_%i_valid",idx);
	    MPI_Info_get_valuelen(info,str2,&keylen,&flag);
	    if(flag){
	      str = calloc(keylen+1,sizeof(char));	    
	      MPI_Info_get(info,str2,keylen,str,&flag);
	      fprintf(stdout,"Process bound to HW resource %i : %s\n",idx,str);
	      free(str);
	    }

	    memset(str2,0,64);
	    sprintf(str2,"mpi_hw_res_%i_naliases",idx);
	    MPI_Info_get_valuelen(info,str2,&keylen,&flag);
	    if(flag){
	      int idx2;
	      int num_aliases = 0;
	      str = calloc(keylen+1,sizeof(char));
	      MPI_Info_get(info,str2,keylen,str,&flag);
	      num_aliases = atoi(str);
	      fprintf(stdout,"The number of aliases for HW resource %i is: %i\n",idx,num_aliases);
	      free(str);		
	      for(idx2 = 0 ; idx2 < num_aliases ; idx2++)
		{
		  memset(str2,0,64);
		  sprintf(str2,"mpi_hw_res_%i_alias_%i",idx,idx2);
		  MPI_Info_get_valuelen(info,str2,&keylen,&flag);
		  if(flag){
		    str = calloc(keylen+1,sizeof(char));
		    MPI_Info_get(info,str2,keylen,str,&flag);
		    fprintf(stdout,"====> HW resource %s is alias to HW resource %i\n",str,idx);
		    free(str);
		  }
		}
	    }
	  }
      }

      fprintf(stdout,"================== Test 7 : MPI_Get_hw_resource_info\n");      
      {
	MPI_Info hw_info;
	char key[MPI_MAX_INFO_KEY] = {0};
	int nkeys = 0;
	
	MPI_Get_hw_resource_info(&hw_info);	
	MPI_Info_get_nkeys(hw_info,&nkeys);

 	fprintf(stdout,"Number of defined keys : %i\n",nkeys);

	for(int i = 0 ; i < nkeys ; i++)
	  {
	    int buflen = 0;
	    int flag = 0;
	    	    
	    MPI_Info_get_nthkey(hw_info,i,key);
	    MPI_Info_get_valuelen(hw_info,key,&buflen,&flag);
	    if(flag){
	      char value[buflen + 1];
	      char *provider = NULL;
	      char *dup_key = NULL;
	      
	      memset(value,0,buflen+1);
	      MPI_Info_get(hw_info,key,buflen,value,&flag);
	      assert(flag);

	      //fprintf(stdout,"[Key = %s, Value = %s]\n",key,value);

	      dup_key = calloc(strlen(key)+1,sizeof(char));
	      memcpy(dup_key,key,(strlen(key)+1)*sizeof(char));
	      
	      provider = strsep(&dup_key,":");	      
	      fprintf(stdout,"Provider %-7s | resource %-7s | binding %-s\n",provider,dup_key+2,value);
	    }
	    
	    /* MPI 4.0 version */
	    /*
	    char *value = NULL;
	    MPI_Info_get_string(hw_info,key,&buflen,value,&flag);
	    if (flag)
	      {
		assert(buflen);
		value = calloc(buflen,sizeof(char));
		MPI_Info_get_string(hw_info,key,&buflen,value,&flag);
		fprintf(stdout,"%s : %s\n",key,value);
		free(value);
	      }
	    */
	  }
      }

      fprintf(stdout,"================== Test 8 : MPI_Get_hw_resource_info + split_type w/GUIDED\n");
      {
	MPI_Comm hw_comm;
	MPI_Info hw_info;	
	
	int   nb_keys   = 0, flag = 0, index = 0;
	int   is_found  = 0, is_restricted = 0; 
	int   valuelen  = strlen("false")+1; //"false" size is larger than "true"	
	char *value     = calloc(valuelen,sizeof(char));
	char *hw_type   = calloc((MPI_MAX_INFO_KEY+1),sizeof(char));
	char *type_name = "hwloc://Package";
	char *provider  = NULL;

	MPI_Get_hw_resource_info(&hw_info);	
	MPI_Info_get_nkeys(hw_info,&nb_keys);
	
	for(index = 0 ; index < nb_keys ; index++){
	  MPI_Info_get_nthkey(hw_info,index,hw_type);
	  MPI_Info_get_string(hw_info,hw_type,&valuelen,value,&flag);	  
	  if(strcmp(hw_type,type_name) == 0){
	    fprintf(stdout,"Found resource type %s (%s)\n",hw_type,type_name);
	    is_found = 1;
	    if(strcmp(value,"true") == 0){
	      is_restricted = 1;
	    }
	    break; // Resource of type type_name was found
	  }
	}

	//The calling MPI process is restricted to a resource
	//of the chosen type
	if((is_found) && (is_restricted)){ 
	  MPI_Info split_info;
	  int rank;
	  int nprocs = 0;
	  
	  MPI_Info_create(&split_info);
	  MPI_Info_set(split_info,"mpi_hw_resource_type",hw_type);
	  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	  MPIX_Comm_split_type(MPI_COMM_WORLD,MPI_COMM_TYPE_HW_GUIDED,rank,split_info,&hw_comm);

	  MPI_Comm_size(hw_comm,&nprocs);
	  fprintf(stdout,"Split done : num procs = %i\n",nprocs);
	}
	/* use hw_comm from this point */  
	
      }

      fprintf(stdout,"================== Test 9 : Testing NUMANode resource\n");
      {
	MPI_Comm hw_comm;
	MPI_Info split_info;
	int rank;
	int nprocs = 0;
	
	MPI_Info_create(&split_info);
	MPI_Info_set(split_info,"mpi_hw_resource_type","hwloc://NUMANode");
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPIX_Comm_split_type(MPI_COMM_WORLD,MPI_COMM_TYPE_HW_GUIDED,rank,split_info,&hw_comm);

	if(hw_comm != MPI_COMM_NULL){
	  MPI_Comm_size(hw_comm,&nprocs);
	  fprintf(stdout,"Split done : num procs = %i\n",nprocs);
	} else {
	  fprintf(stdout,"NULL COMM \n");
	}
      }
      
      MPIX_Hfinalize();
      MPI_Finalize();
    }
  
  return 0;
} 
