#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <alloca.h>
#include <hwloc.h>
#include "mpi.h"

int main(int argc, char **argv)
{
  int num_procs ;
  int rank;
  pid_t *pids;
  int idx,idx2;
  int hwloc_err;
  hwloc_topology_t hwloc_topology;
  hwloc_cpuset_t *set;  
  char buf[32];
  int num_tries = atoi(argv[1]);

  MPI_Init(NULL,NULL);
  MPI_Comm_size(MPI_COMM_WORLD,&num_procs);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  pids = (pid_t *)alloca(num_procs*sizeof(pid_t));
  pids[rank] = getpid();
  MPI_Allgather(MPI_IN_PLACE,0,MPI_DATATYPE_NULL,pids,1,MPI_INT,MPI_COMM_WORLD);

  for(idx = 0 ; idx < num_procs ; idx++)
    fprintf(stdout,"[%i] *** pid[%i] : %i | ",rank,idx,pids[idx]);
  fprintf(stdout,"\n");

  hwloc_err = hwloc_topology_init(&hwloc_topology);
  hwloc_err = hwloc_topology_load(hwloc_topology);

  MPI_Barrier(MPI_COMM_WORLD);

  fprintf(stdout,"Getting binding info from hwloc\n");
      
  set  = (hwloc_cpuset_t *)alloca(num_procs*sizeof(hwloc_cpuset_t ));
  for(idx = 0 ; idx < num_procs ; idx++)
    set[idx] = hwloc_bitmap_alloc();
  
  for(idx2 = 0 ; idx2 < num_tries ; idx2++){
    for(idx = 0 ; idx < num_procs ; idx++){
      hwloc_get_proc_cpubind(hwloc_topology,pids[idx],set[idx],HWLOC_CPUBIND_PROCESS);
      hwloc_bitmap_snprintf(buf,32,set[idx]) ;
      fprintf(stdout,"TRY %i ************ INITIAL PROC BINDING [%i] : %s\n",idx2,idx,buf);	
      memset(buf,0,32);
    }
    fprintf(stdout,"\n");
  }
  

  sleep(5);


  MPI_Finalize();
  
  return EXIT_SUCCESS;
}
