#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <alloca.h>
#include <hwloc.h>
#include "mpi.h"

int main(int argc, char **argv)
{
  int num_procs = atoi(argv[1]);
  pid_t *pids;
  int idx,idx2;
  int hwloc_err;
  hwloc_topology_t hwloc_topology;
  hwloc_cpuset_t current_set;  
  char buf[32];
  int num_tries = 5;

  MPI_Init(NULL,NULL);
  
  pids = (pid_t *)alloca(num_procs*sizeof(pid_t));

  for(idx = 0 ; idx < num_procs ; idx++)
    pids[idx] = atoi(argv[idx+2]); 

  for(idx = 0 ; idx < num_procs ; idx++)
    fprintf(stdout,"===== PID[%i] is %i\n",idx,pids[idx]);

  fprintf(stdout,"Getting binding info from hwloc\n");
  
  hwloc_err = hwloc_topology_init(&hwloc_topology);
  hwloc_err = hwloc_topology_load(hwloc_topology);

  current_set = hwloc_bitmap_alloc();

  for(idx2 = 0 ; idx2 < num_tries ; idx2++){
    for(idx = 0 ; idx < num_procs ; idx++){
	hwloc_get_proc_cpubind(hwloc_topology,pids[idx],current_set,HWLOC_CPUBIND_PROCESS);
	hwloc_bitmap_snprintf(buf,32,current_set) ;
	fprintf(stdout,"TRY %i ************ INITIAL PROC BINDING [%i] : %s\n",idx2,idx,buf);	
	hwloc_bitmap_zero(current_set);
	memset(buf,0,32);
      }
    fprintf(stdout,"\n");
  }


  MPI_Finalize();
  
  return EXIT_SUCCESS;
}
