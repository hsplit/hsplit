#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <sys/time.h>
#include "hsplit.h"

#define NLEVELS 16
#define FLT_MAX 3.402823466e+38F /* max value */
#define FLT_MIN 1.175494351e-38F /* min positive value */

//#define VERBOSE
#define TIME
// Creates an array of random numbers. Each number has a value from 0 - 1
float *create_rand_nums(int num_elements) {
  float *rand_nums = (float *)malloc(sizeof(float) * num_elements);
  assert(rand_nums != NULL);
  int i;
  for (i = 0; i < num_elements; i++) {
    rand_nums[i] = (rand() / (float)RAND_MAX);
  }
  return rand_nums;
}


float func_(float * in_values, int number, char operation) {
  float temp=0;
  switch (operation) {
    
   case '+':
       for(int s=0;s<number;++s) temp+=in_values[s];
       break;

   case '*':
       temp=1;
       for(int s=0;s<number;++s) temp*=in_values[s];
       break;

   case '/':
       temp=1;
       for(int s=0;s<number;++s) temp/=in_values[s];
       break;

   case '-':
       for(int s=0;s<number;++s) temp-=in_values[s];
       break;
   
   case 'M':
       temp = -FLT_MAX;
       for(int s=0;s<number;++s) if(temp<in_values[s]) temp=in_values[s];
       break;

   case 'm':
       temp = FLT_MAX;
       for(int s=0;s<number;++s) if(temp>in_values[s]) temp=in_values[s];
       break;
   }
   return(temp);
}
   


int main(int argc, char** argv) {
  if (argc != 2) {
    //fprintf(stderr, "Usage: avg num_elements_per_proc\n");
    exit(1);
  }

  int num_elements_per_proc = atoi(argv[1]);

  MPI_Init(NULL, NULL);

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Create a random array of elements on all processes.
  srand(time(NULL)*world_rank); // Seed the random number generator of processes uniquely
  float *rand_nums = NULL;
  rand_nums = create_rand_nums(num_elements_per_proc);

  // Sum the numbers locally
  float local_sum = func_(rand_nums, num_elements_per_proc, '+');

#ifdef VERBOSE
  printf("local value[0] of process rank %d is %f \n",world_rank,rand_nums[0]);
#endif
  float *level_result;
  if(world_rank == 0) {
    level_result = (float *)malloc(sizeof(float) * world_size* num_elements_per_proc);
#ifdef VERBOSE1
    fprintf(stdout,"Allocation of vector size %d done\n", world_size);
#endif
    for(int e=0; e<world_size*num_elements_per_proc;++e) level_result[e]=0;
    assert(level_result != NULL);
  } 

  struct timeval last,cur;
  MPI_Barrier(MPI_COMM_WORLD); 
#ifdef TIME
  if(!world_rank){
  gettimeofday(&last, NULL);
  }
#endif 
  MPI_Gather(rand_nums, num_elements_per_proc , MPI_FLOAT, level_result, num_elements_per_proc , MPI_FLOAT, 0 , MPI_COMM_WORLD);
#ifdef TIME
//if(!world_rank)
  {
  gettimeofday(&cur, NULL);
  long temp_global;
  if (cur.tv_usec<last.tv_usec) temp_global=1000000+ cur.tv_usec-last.tv_usec;
  else temp_global = cur.tv_usec-last.tv_usec;
  //printf("Time of collective in level %d is %ld s and %ld u_sec \n", 0, cur.tv_sec-last.tv_sec, temp_global);
  long max_time=0; 
  MPI_Reduce(&temp_global, &max_time, 1, MPI_LONG, MPI_MAX, 0, MPI_COMM_WORLD);
  if (!world_rank) printf("Global max time of collective is %ld s and %ld u_sec \n", cur.tv_sec-last.tv_sec, max_time);
  }
#endif

#ifdef VERBOSE
  if (!world_rank) for(int e=0; e<world_size*num_elements_per_proc;++e) printf("Gathered value in position %d is %f by the process %d \n",e,level_result[e],world_rank);
#endif

  // Clean up
  if(world_rank == 0) free(level_result); 
  free(rand_nums);

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
}
