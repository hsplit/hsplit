#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <assert.h>
#include <time.h>

#include "hsplit.h"

#define NLEVELS 16
#define FLT_MAX 3.402823466e+38F /* max value */
#define FLT_MIN 1.175494351e-38F /* min positive value */

#define VERBOSE
// Creates an array of random numbers. Each number has a value from 0 - 1
float *create_rand_nums(int num_elements) {
  float *rand_nums = (float *)malloc(sizeof(float) * num_elements);
  assert(rand_nums != NULL);
  int i;
  for (i = 0; i < num_elements; i++) {
    rand_nums[i] = (rand() / (float)RAND_MAX);
  }
  return rand_nums;
}


float func_(float * in_values, int number, char operation) {
  float temp=0;
  switch (operation) {
    
   case '+':
       for(int s=0;s<number;++s) temp+=in_values[s];
       break;

   case '*':
       temp=1;
       for(int s=0;s<number;++s) temp*=in_values[s];
       break;

   case '/':
       temp=1;
       for(int s=0;s<number;++s) temp/=in_values[s];
       break;

   case '-':
       for(int s=0;s<number;++s) temp-=in_values[s];
       break;
   
   case 'M':
       temp = -FLT_MAX;
       for(int s=0;s<number;++s) if(temp<in_values[s]) temp=in_values[s];
       break;

   case 'm':
       temp = FLT_MAX;
       for(int s=0;s<number;++s) if(temp>in_values[s]) temp=in_values[s];
       break;
   }
   return(temp);
}
   


int main(int argc, char** argv) {
  if (argc != 2) {
    //fprintf(stderr, "Usage: avg num_elements_per_proc\n");
    exit(1);
  }

  int num_elements_per_proc = atoi(argv[1]);

  MPI_Init(NULL, NULL);
  MPIX_Hinit();

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Create a random array of elements on all processes.
  srand(time(NULL)*world_rank); // Seed the random number generator of processes uniquely
  float *rand_nums = NULL;
  rand_nums = create_rand_nums(num_elements_per_proc);

  // Sum the numbers locally
  float local_sum = func_(rand_nums, num_elements_per_proc, '+');

#ifdef VERBOSE
  printf("local sum of process rank %d is %f \n",world_rank,local_sum);
#endif

  // create the hierarchy // 
  MPI_Info info;
  MPI_Info infos[NLEVELS];
  int rank,idx;
  MPI_Comm newcomm;
  MPI_Comm out_comm;
  MPI_Comm levels_comm[NLEVELS];
  int flag;
  char comm_name[MPI_MAX_INFO_VAL];
  int scomm_num;
  int scomm_rank;
  char *resource_type = NULL;
   
  //fprintf(stdout,"================== Hierarchy of communicators ================\n");
  MPI_Comm in_comm = MPI_COMM_WORLD;
  idx = 0;
  while(in_comm != MPI_COMM_NULL)
        {
         levels_comm[idx] = in_comm;      
         MPI_Comm_rank(in_comm,&rank);
         MPI_Info_create(&infos[idx]);	
	 MPIX_Comm_split_type(in_comm,MPI_COMM_TYPE_HW_SUBDOMAIN,rank,infos[idx],&out_comm);
        
	 /* Eventually use this to retrieve the info */
	 //MPI_Comm_get_info(out_comm,&info[idx]);
	 if (out_comm != MPI_COMM_NULL && !world_rank){	   
	   MPIX_Get_hw_resource_info(levels_comm[idx],&scomm_num,&scomm_rank,&resource_type,infos[idx]);
	   /* eventually use this version */
	   //MPIX_Get_hierarchy_info(out_comm,&scomm_num,&scomm_rank,&resource_type);
#ifdef VERBOSE
	   fprintf(stdout,"========== SubLevel %i info : \n",idx);
	   fprintf(stdout,"=== Number of subcomms : %i\n",scomm_num);
	   fprintf(stdout,"=== Subcomm rank : %i\n",scomm_rank);
	   fprintf(stdout,"=== Subcomm type : %s\n",resource_type);
#endif
	 }

	 assert(++idx < NLEVELS);
	 in_comm = out_comm;
       }             

  int set_level;
  int levels_rank[NLEVELS]; 
  int levels_size[NLEVELS];
  float reduce_level[NLEVELS];
  float *level_result[NLEVELS];
  
  reduce_level[idx] = local_sum; 
  for(set_level=idx-1;set_level>=0;--set_level)
  {
  MPI_Comm_rank(levels_comm[set_level], &levels_rank[set_level]);
  MPI_Comm_size(levels_comm[set_level], &levels_size[set_level]);

#ifdef VERBOSE  
  printf("Level %d == %d process| my rank is %d\n", set_level, levels_size[set_level], levels_rank[set_level]);
#endif

  if (levels_rank[set_level] == 0) {
    level_result[set_level] = (float *)malloc(sizeof(float) * levels_size[set_level]);
    for(int e=0; e<levels_size[set_level];++e) level_result[set_level][e]=0;
    assert(level_result[set_level] != NULL);
  } 
  
  MPI_Gather(&(reduce_level[set_level+1]), 1, MPI_FLOAT, level_result[set_level], 1, MPI_FLOAT, 0 , levels_comm[set_level]);
  reduce_level[set_level]=0;
  if (levels_rank[set_level] == 0) 
       {
         switch (set_level) 
         {

          case 0 : 
               reduce_level[set_level] = func_(level_result[set_level],levels_size[set_level],'+');
               break; 
          case 1 : 
               reduce_level[set_level] = func_(level_result[set_level],levels_size[set_level],'/');
               break; 
          case 2: 
               reduce_level[set_level] = func_(level_result[set_level],levels_size[set_level],'M');
               break; 
          case 4: 
               reduce_level[set_level] = func_(level_result[set_level],levels_size[set_level],'m');
               break; 
          default: 
	       reduce_level[set_level] = func_(level_result[set_level],levels_size[set_level],'*');
               break; 
         }
#ifdef VERBOSE
         printf(" Reduced value is %f \n", reduce_level[set_level]);
#endif
       }
  }

  // Clean up
  for(set_level=idx-1;set_level>=0;--set_level) if (levels_rank[set_level] == 0) free(level_result[set_level]); 
  free(rand_nums);

  MPI_Barrier(MPI_COMM_WORLD);
  MPIX_Hfinalize();
  MPI_Finalize();
}
