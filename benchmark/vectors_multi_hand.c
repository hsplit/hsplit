#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <sys/time.h>
#include "hsplit.h"

#define NLEVELS 16
#define FLT_MAX 3.402823466e+38F /* max value */
#define FLT_MIN 1.175494351e-38F /* min positive value */

//#define VERBOSE2
//#define TIME
#define GLOBAL_TIME
#define H_LEVEL_MAX 10

int H_level_amount=0;
MPI_Comm H_levels_comm_root[H_LEVEL_MAX];
MPI_Info H_comm_root_infos[H_LEVEL_MAX];
static int H_size_levels_comm_root[H_LEVEL_MAX];


int Hcoll_init(MPI_Comm input_comm)
{
MPIX_Hinit();

MPI_Comm out_comm;
MPI_Comm root_comm=input_comm;
MPI_Comm in_comm = input_comm;
int idx = 0;
int world_rank;
int root_size,root_rank; 
MPI_Comm_rank(input_comm, &world_rank);
H_size_levels_comm_root[idx]=1;
int flag;
char comm_name[MPI_MAX_INFO_VAL];
int scomm_num;
int scomm_rank;
char *resource_type = NULL;

while(in_comm != MPI_COMM_NULL)
        {
         MPI_Info_create(&H_comm_root_infos[idx]);
         MPIX_Comm_hsplit_with_roots(in_comm, MPI_COMM_TYPE_HW_UNGUIDED ,H_comm_root_infos[idx], &out_comm, &root_comm);
         if(root_comm != MPI_COMM_NULL)
         {
           H_levels_comm_root[idx] = root_comm;
           MPI_Comm_rank(H_levels_comm_root[idx], &root_rank);
           MPI_Comm_size(H_levels_comm_root[idx], &root_size);
           H_size_levels_comm_root[idx+1]=H_size_levels_comm_root[idx]*root_size;
           //printf(" oooo///oooo Level %d : %d process| my rank is %d\n", idx, root_size, root_rank);

         }else 
         {
           H_levels_comm_root[idx] = MPI_COMM_NULL;
           //printf("oooo//oooo MPI_COMM_NULL \n");
         }

#ifdef VERBOSE2
	 if (out_comm != MPI_COMM_NULL && !world_rank){	   
	   //MPIX_Get_hierarchy_info(H_levels_comm_root[idx],&scomm_num,&scomm_rank,&resource_type,H_comm_root_infos[idx]);
	   MPIX_Get_hlevel_info(H_levels_comm_root[idx],&scomm_num,&scomm_rank,&resource_type,H_comm_root_infos[idx]);
           fprintf(stdout,"========== SubLevel %i info : \n",idx);
	   fprintf(stdout,"=== Number of subcomms : %i\n",scomm_num);
	   fprintf(stdout,"=== Subcomm rank : %i\n",scomm_rank);
	   fprintf(stdout,"=== Subcomm type : %s\n",resource_type);
	 }
#endif


	 ++idx;
	 in_comm = out_comm;
       }
//MPI_Bcast(H_size_levels_comm_root, idx, MPI_INT, 0, MPI_COMM_WORLD);
H_level_amount=idx-1;
return(0);
}


int Hcoll_finalize()
{
MPIX_Hfinalize();
}

void Hcoll_show_hierarchy(MPI_Comm *H_levels_comm_root, int H_level_amount, int *H_size_levels_comm_root)
{
int world_rank,world_size;
MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
MPI_Comm_size(MPI_COMM_WORLD, &world_size);
int flag;
char comm_name[MPI_MAX_INFO_VAL];
int scomm_num;
int scomm_rank;
char *resource_type = NULL;

printf("---------------------------------------------------------------------------- \n", world_rank, H_level_amount);
printf("Process: %d|Hierarchy level amount : %d \n", world_rank, H_level_amount);
for(int idx=0; idx<H_level_amount; idx++) 
  { 
    int root_rank,root_size;
    printf("Process: %d|Communicators amount in level %d is %d \n",world_rank, idx, H_size_levels_comm_root[idx]);
#ifdef VERBOSE2
    if (H_levels_comm_root[idx] != MPI_COMM_NULL)
         {
           MPI_Comm_rank(H_levels_comm_root[idx], &root_rank);
           MPI_Comm_size(H_levels_comm_root[idx], &root_size);
           printf("Communicator size in level %d is %d \n",idx,root_size);
           MPIX_Get_hlevel_info(H_levels_comm_root[idx],&scomm_num,&scomm_rank,&resource_type,H_comm_root_infos[idx]);
           fprintf(stdout,"========== SubLevel %i info : \n",idx);
           fprintf(stdout,"=== Number of subcomms : %i\n",scomm_num);
           fprintf(stdout,"=== Subcomm rank : %i\n",scomm_rank);
           fprintf(stdout,"=== Subcomm type : %s\n",resource_type);
         }
     else printf("comm_root of process %d is MPI_COMM_NULL \n",world_rank);
#endif
  }
} 

// Creates an array of random numbers. Each number has a value from 0 - 1
float *create_rand_nums(int num_elements)
{
       float *rand_nums = (float *)malloc(sizeof(float) * num_elements);
       assert(rand_nums != NULL);
       int i;
       for (i = 0; i < num_elements; i++)
       {
            rand_nums[i] = (rand() / (float)RAND_MAX);
       }

       return rand_nums;
}


float func_norm(float *in_values, const float *vector, int number)
{
      float temp=0;
      for(int s=0;s<number;++s) temp+=in_values[s]*vector[s];
      temp = temp / number;
      return(temp);
}
      


int Comm_split_config(MPI_Comm input_comm, MPI_Comm *output_comms, MPI_Comm *output_root_comms, int levels, int *num_comms)
{
    int rc = 0;
    int rank,size,rank_out,size_out, rank_root,size_root;
    int world_rank, world_size; 
    int levels_local=levels;
    
    MPI_Comm *comms=output_comms;
    MPI_Comm *root_comms=output_root_comms;

    comms[levels_local+1]=input_comm;
    MPI_Comm_rank(comms[levels_local+1], &world_rank);
    MPI_Comm_size(comms[levels_local+1], &world_size);

    while (levels_local)
    {
           MPI_Barrier(MPI_COMM_WORLD);
            
           MPI_Comm_rank(comms[levels_local+1], &rank);
           MPI_Comm_size(comms[levels_local+1], &size);
           int color = (int) (rank / num_comms[levels_local-1]);
           //if (!rank) printf("IT %d | num comms %d | size %d \n", levels_local, num_comms[levels_local-1], size);

           rc = MPI_Comm_split(comms[levels_local+1], color, rank, &comms[levels_local]);
          
           MPI_Comm_rank(comms[levels_local], &rank_out);
           MPI_Comm_size(comms[levels_local], &size_out);
 
           //if (!rank_out) 
           //    printf("Level %d| Num comms %d| rank %d| size %d| color %d \n", 
           //           levels_local, num_comms[levels_local-1], rank_out, size_out, color);

           if (rank_out) 
               color = MPI_UNDEFINED;
           else 
               color = 0;
           
           rc = MPI_Comm_split(comms[levels_local+1], color, rank_out, &root_comms[levels-levels_local]);
           
           if (root_comms[levels-levels_local] == MPI_COMM_NULL )
               {
               size_root=-1;
               //printf("error split\n");
               }
           else
               {
               MPI_Comm_rank(root_comms[levels-levels_local], &rank_root);
               MPI_Comm_size(root_comms[levels-levels_local], &size_root);
               //printf("It %d| Input comm: %d proc| Num comms: %d| Output comm: %d proc| Root comm: %d proc| Rank out %d\n",
               //            levels_local, size, num_comms[levels_local-1], size_out, size_root, rank_out);

               } 
           
           levels_local--; 
    }
    
    return(rc);
}

float *func_vec(float *in_values, const float *vector, int number, int size)
{
      float *vec = (float*) malloc( sizeof(float)*size);
      memset(vec,0,size);
      for(int s=0;s<size;++s)
      { 
          for(int x=0;x<number;++x) 
              vec[s]=vec[s]+in_values[s+(size*x)];
        
          vec[s] = vec[s]*vector[s];
      }
      return(vec);
}

struct timeval global_time_start(int world_rank)
{
    struct timeval last_global;
    gettimeofday(&last_global, NULL);
    return last_global;
}

void global_time_end(int world_rank, struct timeval last_global)
{
  struct timeval cur_global;
  gettimeofday(&cur_global, NULL);
  long temp_global;
  
  if (cur_global.tv_usec<last_global.tv_usec) 
      temp_global=1000000+ cur_global.tv_usec-last_global.tv_usec;
  else 
      temp_global = cur_global.tv_usec-last_global.tv_usec;
  
  //printf("Global time of collective is %ld s and %ld u_sec \n", cur_global.tv_sec-last_global.tv_sec, temp_global);
  long max_time=0; 
  MPI_Reduce(&temp_global, &max_time, 1, MPI_LONG, MPI_MAX, 0, MPI_COMM_WORLD);
  
  if (!world_rank) 
      printf("Global max time of collective is %ld s and %ld u_sec \n", 
              cur_global.tv_sec-last_global.tv_sec, max_time);
 
}

int main(int argc, char** argv)
{
    if (argc != 2)
    {
       //fprintf(stderr, "Usage: avg num_elements_per_proc\n");
       exit(1);
    }

    int num_elements_per_proc = atoi(argv[1]);
    int world_rank;
    int world_size;
 
    float *rand_nums = NULL;
    const float *rand_local_vector = NULL;
    float local_sum;
    float *first_vec;
 
    MPI_Init(NULL, NULL);
 
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Create a random array of elements on all processes.
    srand(time(NULL)*world_rank); // Seed the random number generator of processes uniquely
    rand_nums = create_rand_nums(num_elements_per_proc);

    // Create the constant array on root process
    rand_local_vector = create_rand_nums(num_elements_per_proc);

    // Sum the numbers locally
    local_sum = func_norm(rand_nums, rand_local_vector, num_elements_per_proc);
    first_vec = func_vec(rand_nums, rand_local_vector, 1,num_elements_per_proc);

#ifdef VERBOSE
    if (!world_rank)
    for(int s=0;s<num_elements_per_proc; ++s)
        printf("first vec of process rank %d is %f \n",world_rank,first_vec[s]);
#endif

    MPI_Barrier(MPI_COMM_WORLD);
    /* here the config need to be like this: 
     * (numproc_lastlevel=num avail proc, numproc, num_proc) */
    int level_c = 4;
    int num_comms[4]={8,4,4,3};
    MPI_Comm output_comms[level_c+1];
    MPI_Comm output_root_comms[level_c];
    Comm_split_config(MPI_COMM_WORLD, &output_comms, &output_root_comms, level_c, num_comms);

    //printf(" Comms @: %p| Root comm @: %p \n", output_comms, output_root_comms);
 
    //MPI_Barrier(MPI_COMM_WORLD); 
    //Hcoll_init(MPI_COMM_WORLD);
    //Hcoll_show_hierarchy();
    //MPI_Barrier(MPI_COMM_WORLD);
        
    //fprintf(stdout," idx max %d \n", idx);
    int set_level;
    int levels_rank[NLEVELS]; 
    int levels_size[NLEVELS];
    float *reduce_level[NLEVELS];
    float *level_result[NLEVELS];
    int idx = level_c;
    reduce_level[idx] = first_vec;
   
    MPI_Barrier(MPI_COMM_WORLD);

#ifdef GLOBAL_TIME 
    struct timeval last_global = global_time_start(world_rank);
#endif 
 
   for(set_level=idx-1;set_level>=0;--set_level)
    {
        //MPI_Barrier(MPI_COMM_WORLD);
        if (output_root_comms[set_level] != MPI_COMM_NULL )
        {
            MPI_Comm_rank(output_root_comms[set_level], &levels_rank[set_level]);
            MPI_Comm_size(output_root_comms[set_level], &levels_size[set_level]);

#ifdef VERBOSE2 
            printf("Level %d == %d process| my rank is %d\n", set_level, levels_size[set_level], levels_rank[set_level]);
#endif

            if(levels_rank[set_level] == 0)
            { 
               level_result[set_level] = (float *)malloc(sizeof(float) * levels_size[set_level]*num_elements_per_proc);
               memset(level_result[set_level],0,levels_size[set_level]*num_elements_per_proc);
#ifdef VERBOSE2
               fprintf(stdout,"Level %d |Allocation of vector size %d done\n", set_level, levels_size[set_level]*num_elements_per_proc);
#endif
            }


#ifdef TIME
            struct timeval last,cur; 
            if(!world_rank)
            {
               gettimeofday(&last, NULL);
            }
#endif 

            MPI_Gather(reduce_level[set_level+1], num_elements_per_proc, MPI_FLOAT, level_result[set_level], num_elements_per_proc, MPI_FLOAT, 0 , output_root_comms[set_level]);

#ifdef TIME
            if(!world_rank)
            {
               gettimeofday(&cur, NULL);
               long temp;
               if (cur.tv_usec<last.tv_usec) temp=1000000+ cur.tv_usec-last.tv_usec;
               else temp = cur.tv_usec-last.tv_usec;
               printf("Time of collective in level %d is %ld s and %ld u_sec \n", 
                     set_level, cur.tv_sec-last.tv_sec, temp);
            }
#endif

            if (levels_rank[set_level] == 0) 
            {
                reduce_level[set_level] = func_vec(level_result[set_level], rand_local_vector, 
                                         levels_size[set_level], num_elements_per_proc);
            }

#ifdef VERBOSE2
            if(levels_rank[set_level] == 0)
               printf("Computed value in level %d is %f by the process %d \n",
                     set_level,reduce_level[set_level][0],levels_rank[set_level]);
#endif
       }
    }

#ifdef GLOBAL_TIME
    global_time_end(world_rank, last_global);
#endif


  // Clean up
  free(rand_nums);

  //Hcoll_finalize();
  MPI_Finalize();
}
