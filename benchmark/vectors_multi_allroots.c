#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <sys/time.h>
#include "hsplit.h"

#define NLEVELS 16
#define FLT_MAX 3.402823466e+38F /* max value */
#define FLT_MIN 1.175494351e-38F /* min positive value */

#define VERBOSE2
#define TIME
#define H_LEVEL_MAX 10

int H_level_amount=0;
MPI_Comm H_levels_comm_root[H_LEVEL_MAX];
MPI_Info H_comm_root_infos[H_LEVEL_MAX];
static int H_size_levels_comm_root[H_LEVEL_MAX];


int Hcoll_init(MPI_Comm input_comm)
{
MPIX_Hinit();

MPI_Comm out_comm;
MPI_Comm root_comm=input_comm;
MPI_Comm in_comm = input_comm;
int idx = 0;
int world_rank;
int root_size,root_rank; 
MPI_Comm_rank(input_comm, &world_rank);
H_size_levels_comm_root[idx]=1;
int flag;
char comm_name[MPI_MAX_INFO_VAL];
int scomm_num;
int scomm_rank;
char *resource_type = NULL;

while(in_comm != MPI_COMM_NULL)
        {
         MPI_Info_create(&H_comm_root_infos[idx]);
         MPIX_Comm_hsplit_with_allroots(in_comm, H_comm_root_infos[idx], &out_comm, &root_comm);
         if(root_comm != MPI_COMM_NULL)
         {
           H_levels_comm_root[idx] = root_comm;
           MPI_Comm_rank(H_levels_comm_root[idx], &root_rank);
           MPI_Comm_size(H_levels_comm_root[idx], &root_size);
           H_size_levels_comm_root[idx+1]=H_size_levels_comm_root[idx]*root_size;
           //printf(" oooo///oooo Level %d : %d process| my rank is %d\n", idx, root_size, root_rank);

         }else 
         {
           H_levels_comm_root[idx] = MPI_COMM_NULL;
           //printf("oooo//oooo MPI_COMM_NULL \n");
         }

#ifdef VERBOSE2
	 if (out_comm != MPI_COMM_NULL && !world_rank){	   
	   //MPIX_Get_hierarchy_info(H_levels_comm_root[idx],&scomm_num,&scomm_rank,&resource_type,H_comm_root_infos[idx]);
	   MPIX_Get_hw_resource_info(H_levels_comm_root[idx],&scomm_num,&scomm_rank,&resource_type,H_comm_root_infos[idx]);
           fprintf(stdout,"========== SubLevel %i info : \n",idx);
	   fprintf(stdout,"=== Number of subcomms : %i\n",scomm_num);
	   fprintf(stdout,"=== Subcomm rank : %i\n",scomm_rank);
	   fprintf(stdout,"=== Subcomm type : %s\n",resource_type);
	 }
#endif


	 ++idx;
	 in_comm = out_comm;
       }
//MPI_Bcast(H_size_levels_comm_root, idx, MPI_INT, 0, MPI_COMM_WORLD);
H_level_amount=idx-1;
return(0);
}


int Hcoll_finalize()
{
MPIX_Hfinalize();
}

void Hcoll_show_hierarchy()
{
int world_rank,world_size;
MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
MPI_Comm_size(MPI_COMM_WORLD, &world_size);
int flag;
char comm_name[MPI_MAX_INFO_VAL];
int scomm_num;
int scomm_rank;
char *resource_type = NULL;

printf("---------------------------------------------------------------------------- \n", world_rank, H_level_amount);
printf("Process: %d|Hierarchy level amount : %d \n", world_rank, H_level_amount);
for(int idx=0; idx<H_level_amount; idx++) 
  { 
    int root_rank,root_size;
    printf("Process: %d|Communicators amount in level %d is %d \n",world_rank, idx, H_size_levels_comm_root[idx]);
#ifdef VERBOSE2
    if (H_levels_comm_root[idx] != MPI_COMM_NULL)
         {
           MPI_Comm_rank(H_levels_comm_root[idx], &root_rank);
           MPI_Comm_size(H_levels_comm_root[idx], &root_size);
           printf("Communicator size in level %d is %d \n",idx,root_size);
           MPIX_Get_hw_resource_info(H_levels_comm_root[idx],&scomm_num,&scomm_rank,&resource_type,H_comm_root_infos[idx]);
           fprintf(stdout,"========== SubLevel %i info : \n",idx);
           fprintf(stdout,"=== Number of subcomms : %i\n",scomm_num);
           fprintf(stdout,"=== Subcomm rank : %i\n",scomm_rank);
           fprintf(stdout,"=== Subcomm type : %s\n",resource_type);
         }
     else printf("comm_root of process %d is MPI_COMM_NULL \n",world_rank);
#endif
  }
} 


// Creates an array of random numbers. Each number has a value from 0 - 1
float *create_rand_nums(int num_elements)
{
       float *rand_nums = (float *)malloc(sizeof(float) * num_elements);
       assert(rand_nums != NULL);
       int i;
       for (i = 0; i < num_elements; i++)
       {
            rand_nums[i] = (rand() / (float)RAND_MAX);
       }

       return rand_nums;
}


float func_norm(float *in_values, const float *vector, int number)
{
      float temp=0;
      for(int s=0;s<number;++s) temp+=in_values[s]*vector[s];
      temp = temp / number;
      return(temp);
}
      

int main(int argc, char** argv)
{
    if (argc != 2)
    {
       //fprintf(stderr, "Usage: avg num_elements_per_proc\n");
       exit(1);
    }

    int num_elements_per_proc = atoi(argv[1]);
    int world_rank;
    int world_size;
 
    float *rand_nums = NULL;
    const float *rand_local_vector = NULL;
    float local_sum;
 
    MPI_Init(NULL, NULL);
 
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Create a random array of elements on all processes.
    srand(time(NULL)*world_rank); // Seed the random number generator of processes uniquely
    rand_nums = create_rand_nums(num_elements_per_proc);

    // Create the constant array on root process
    rand_local_vector = create_rand_nums(num_elements_per_proc);

    // Sum the numbers locally
    local_sum = func_norm(rand_nums, rand_local_vector, num_elements_per_proc);

#ifdef VERBOSE2
    printf("local sum of process rank %d is %f \n",world_rank,local_sum);
#endif

    Hcoll_init(MPI_COMM_WORLD);
    //Hcoll_show_hierarchy();
    MPI_Barrier(MPI_COMM_WORLD);
        
  //fprintf(stdout," idx max %d \n", idx);
  int set_level;
  int levels_rank[NLEVELS]; 
  int levels_size[NLEVELS];
  float reduce_level[NLEVELS];
  float *level_result[NLEVELS];

  int idx = H_level_amount;
  reduce_level[idx] = local_sum;
  for(set_level=idx-1;set_level>=0;--set_level)
  {
      if (H_levels_comm_root[set_level] != MPI_COMM_NULL )
      {
          MPI_Comm_rank(H_levels_comm_root[set_level], &levels_rank[set_level]);
          MPI_Comm_size(H_levels_comm_root[set_level], &levels_size[set_level]);

#ifdef VERBOSE2 
          printf("Level %d == %d process| my rank is %d\n", set_level, levels_size[set_level], levels_rank[set_level]);
#endif

          //if(levels_rank[set_level] == 0)
          { 
             level_result[set_level] = (float *)malloc(sizeof(float) * levels_size[set_level]);
             assert(level_result[set_level] != NULL);

#ifdef VERBOSE1
             fprintf(stdout,"Allocation of vector size %d done\n", levels_size[set_level]);
#endif

       //      for(int e=0; e<levels_size[set_level];++e) level_result[set_level][e]=0;
          }


#ifdef TIME
          struct timeval last,cur; 
          if(!world_rank)
          {
             gettimeofday(&last, NULL);
          }
#endif 

          //MPI_Gather(&(reduce_level[set_level+1]), 1, MPI_FLOAT, level_result[set_level], 1, MPI_FLOAT, 0 , H_levels_comm_root[set_level]);
          MPI_Allgather(&(reduce_level[set_level+1]), 1, MPI_FLOAT, level_result[set_level], 1, MPI_FLOAT, H_levels_comm_root[set_level]);

          //MPI_Barrier(MPI_COMM_WORLD);
#ifdef TIME
         if(!world_rank)
         {
            gettimeofday(&cur, NULL);
            long temp;
            if (cur.tv_usec<last.tv_usec) temp=1000000+ cur.tv_usec-last.tv_usec;
            else temp = cur.tv_usec-last.tv_usec;
            printf("Time of collective in level %d is %ld s and %ld u_sec \n", set_level, cur.tv_sec-last.tv_sec, temp);
         }
#endif

         reduce_level[set_level]=0;
         //if (levels_rank[set_level] == 0) 
         {
             reduce_level[set_level] = func_norm(level_result[set_level], rand_local_vector, levels_size[set_level]);
         }

#ifdef VERBOSE2
         printf("Computed value in level %d is %f by the process %d \n",set_level,reduce_level[set_level],levels_rank[set_level]);
#endif
         }
     }


  // Clean up
  free(rand_nums);

  MPI_Barrier(MPI_COMM_WORLD);
  Hcoll_finalize();
  MPI_Finalize();
}
