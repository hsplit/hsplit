#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <sys/time.h>
#include "hsplit.h"

#define NLEVELS 16
#define FLT_MAX 3.402823466e+38F /* max value */
#define FLT_MIN 1.175494351e-38F /* min positive value */

//#define VERBOSE
//#define TIME
#define GLOBAL_TIME
// Creates an array of random numbers. Each number has a value from 0 - 1
float *create_rand_nums(int num_elements) {
  float *rand_nums = (float *)malloc(sizeof(float) * num_elements);
  assert(rand_nums != NULL);
  int i;
  for (i = 0; i < num_elements; i++) {
    rand_nums[i] = (rand() / (float)RAND_MAX);
  }
  return rand_nums;
}


float func_(float * in_values, int number, char operation) {
  float temp=0;
  switch (operation) {
    
   case '+':
       for(int s=0;s<number;++s) temp+=in_values[s];
       break;

   case '*':
       temp=1;
       for(int s=0;s<number;++s) temp*=in_values[s];
       break;

   case '/':
       temp=1;
       for(int s=0;s<number;++s) temp/=in_values[s];
       break;

   case '-':
       for(int s=0;s<number;++s) temp-=in_values[s];
       break;
   
   case 'M':
       temp = -FLT_MAX;
       for(int s=0;s<number;++s) if(temp<in_values[s]) temp=in_values[s];
       break;

   case 'm':
       temp = FLT_MAX;
       for(int s=0;s<number;++s) if(temp>in_values[s]) temp=in_values[s];
       break;
   }
   return(temp);
}
   


int main(int argc, char** argv) {
  if (argc != 2) {
    //fprintf(stderr, "Usage: avg num_elements_per_proc\n");
    exit(1);
  }

  int num_elements_per_proc = atoi(argv[1]);

  MPI_Init(NULL, NULL);
  MPIX_Hinit();

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Create a random array of elements on all processes.
  srand(time(NULL)*world_rank); // Seed the random number generator of processes uniquely
  float *rand_nums = NULL;
  rand_nums = create_rand_nums(num_elements_per_proc);

  // Sum the numbers locally
  float local_sum = func_(rand_nums, num_elements_per_proc, '+');

#ifdef VERBOSE
  printf("values in position 0 of process rank %d is %f \n",world_rank,rand_nums[0]);
#endif

  // create the hierarchy // 
  MPI_Info info;
  MPI_Info infos[NLEVELS];
  int rank,idx;
  MPI_Comm newcomm;
  MPI_Comm out_comm, root_comm=MPI_COMM_WORLD;
  MPI_Comm levels_comm[NLEVELS],levels_comm_r[NLEVELS];
  int flag;
  char comm_name[MPI_MAX_INFO_VAL];
  int scomm_num;
  int scomm_rank;
  char *resource_type = NULL;
  int root_rank, root_size;
 
  //fprintf(stdout,"================== Hierarchy of communicators ================\n");
  MPI_Comm in_comm = MPI_COMM_WORLD;
  idx = 0;
  while(in_comm != MPI_COMM_NULL)
        {
         levels_comm[idx] = in_comm;
         MPI_Comm_rank(in_comm,&rank);
         MPI_Info_create(&infos[idx]);	
	 //MPIX_Comm_split_type(in_comm,MPI_COMM_TYPE_HIERARCHICAL,rank,infos[idx],&out_comm);
         MPIX_Comm_hsplit_with_roots(in_comm, MPI_COMM_TYPE_HW_UNGUIDED ,infos[idx], &out_comm, &root_comm);
         if (root_comm != MPI_COMM_NULL){
           levels_comm_r[idx] = root_comm;
           //if (!world_rank)
           {
           MPI_Comm_rank(levels_comm_r[idx], &root_rank);
           MPI_Comm_size(levels_comm_r[idx], &root_size);
#ifdef VERBOSE1  
           printf(" oooo///oooo Level %d : %d process| my rank is %d\n", idx, root_size, root_rank);
#endif
           }
         }else 
           {
#ifdef VERBOSE1
             fprintf(stdout, "oooo///oooo Invalid Communicator \n");
#endif
             levels_comm_r[idx] = MPI_COMM_NULL;
           }
                 
         /* Eventually use this to retrieve the info */
	 //MPI_Comm_get_info(out_comm,&info[idx]);
	 if (out_comm != MPI_COMM_NULL && !world_rank){	   
	   MPIX_Get_hw_resource_info(levels_comm_r[idx],&scomm_num,&scomm_rank,&resource_type,infos[idx]);
	              /* eventually use this version */
	   //MPIX_Get_hierarchy_info(out_comm,&scomm_num,&scomm_rank,&resource_type);
           
#ifdef VERBOSE1
	   fprintf(stdout,"========== SubLevel %i info : \n",idx);
	   fprintf(stdout,"=== Number of subcomms : %i\n",scomm_num);
	   fprintf(stdout,"=== Subcomm rank : %i\n",scomm_rank);
	   fprintf(stdout,"=== Subcomm type : %s\n",resource_type);
#endif
	 }

	 assert(++idx < NLEVELS);
	 in_comm = out_comm;
       }

            
  //fprintf(stdout," idx max %d \n", idx);
  int set_level;
  int levels_rank[NLEVELS]; 
  int levels_size[NLEVELS];
  float *level_result[NLEVELS];
  int size_vector = 1;
  level_result[idx] = (float *)malloc(sizeof(float)*num_elements_per_proc ); 
  level_result[idx-1] = rand_nums;
  struct timeval last_global,cur_global; 
  struct timeval last,cur;

  MPI_Barrier(MPI_COMM_WORLD);   

#ifdef GLOBAL_TIME
  //if(!world_rank)
  {
  gettimeofday(&last_global, NULL);
  }
#endif

 
  for(set_level=idx-1;set_level>=0;--set_level)
  {
  if (levels_comm_r[set_level] != MPI_COMM_NULL ){
  MPI_Comm_rank(levels_comm_r[set_level], &levels_rank[set_level]);
  MPI_Comm_size(levels_comm_r[set_level], &levels_size[set_level]);

#ifdef VERBOSE  
  printf("Level %d == %d process| my rank is %d\n", set_level, levels_size[set_level], levels_rank[set_level]);
#endif
  
  if(levels_rank[set_level] == 0) {
    level_result[set_level] = (float *)malloc(sizeof(float) * (levels_size[set_level]*num_elements_per_proc*size_vector));
#ifdef VERBOSE
    fprintf(stdout,"Allocation of vector size %d done\n", levels_size[set_level]*num_elements_per_proc*size_vector);
#endif
    //for(int e=0; e<(levels_size[set_level]*num_elements_per_proc*size_vector);++e) level_result[set_level][e]=0;
    assert(level_result[set_level] != NULL);
  } 

#ifdef TIME
  if(!levels_rank[set_level])
  {
  gettimeofday(&last, NULL);
  }
#endif 

  MPI_Gather(level_result[set_level+1], num_elements_per_proc*size_vector, MPI_FLOAT, level_result[set_level], num_elements_per_proc*size_vector, MPI_FLOAT, 0 , levels_comm_r[set_level]);

#ifdef TIME
  if(!levels_rank[set_level]){
  gettimeofday(&cur, NULL);
  long temp;
  if (cur.tv_usec<last.tv_usec) temp=1000000+ cur.tv_usec-last.tv_usec;
  else temp = cur.tv_usec-last.tv_usec;
  printf("Time of collective in level %d is %ld s and %ld u_sec \n", set_level, cur.tv_sec-last.tv_sec, temp);
  }
#endif

  size_vector = size_vector*levels_size[set_level];
    }    
  }

#ifdef GLOBAL_TIME
  //if(!world_rank)
  {
  gettimeofday(&cur_global, NULL);
  long temp_global;
  if (cur_global.tv_usec<last_global.tv_usec) temp_global=1000000+ cur_global.tv_usec-last_global.tv_usec;
  else temp_global = cur_global.tv_usec-last_global.tv_usec;
  //printf("Global time of collective is %ld s and %ld u_sec \n", cur_global.tv_sec-last_global.tv_sec, temp_global);
  long max_time=0; 
  MPI_Reduce(&temp_global, &max_time, 1, MPI_LONG, MPI_MAX, 0, MPI_COMM_WORLD);
  if (!world_rank) printf("Global max time of collective is %ld s and %ld u_sec \n", cur_global.tv_sec-last_global.tv_sec, max_time);
  }
#endif


#ifdef VERBOSE
  if (!world_rank) for(int e=0; e<num_elements_per_proc*world_size;++e) printf("Gathered value in position %d is %f by the process %d \n",e,level_result[0][e],0); 
#endif

  // Clean up
  for(set_level=idx-1;set_level>=0;--set_level) if (levels_comm_r[set_level] != MPI_COMM_NULL )  if(levels_rank[set_level] == 0) free(level_result[set_level]); 
  free(rand_nums);

  MPI_Barrier(MPI_COMM_WORLD);
  MPIX_Hfinalize();
  MPI_Finalize();
}
